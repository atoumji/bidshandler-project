""" This module is used to provide tools to the BIDS viewer package.

Classes:
    Tools:
        This class is used to provide tools to the BIDS viewer package.

Functions:
    contain_entities():
        Check if the metadata entities are all comprised in the file
    order_files():
        Order the files by the distance between the file and the files in the list
"""

import json
import os
from re import search
import platform


def get_entities(experiments_files, project_directory):
    """ This method is used to get the entities of the experiments.

        The entities of the experiments are found.
        :param: list
        A list of the experiments files paths.
        :return: dict
        A dict containing the entities of the experiments in the form {subject : {session : [filepath, ...]}, ...}.
        Where the third level is the file, the second level is the session and the first level is the subject.
    """
    entities = {}
    for experiment in experiments_files:
        directory = experiment.split(project_directory)[1].split('/')
        sub = ''
        ses = ''
        for info in directory[:-1]:
            if 'sub-' in info:
                sub = info
                if sub not in entities:
                    entities[sub] = {}
            elif 'ses-' in info:
                ses = info
                if ses not in entities[sub]:
                    entities[sub][ses] = []
        if ses == '':
            ses = 'ses-1'
            if ses not in entities[sub]:
                entities[sub][ses] = []
        entities[sub][ses].append(experiment)
    return entities


def get_entities_single_file(file):
    """ This method is used to get the entities of a file.

        The entities of a file are found.
        :param: str
        The file path.
        :return: dict
        A dict containing the entities of the file in the form {entity : entity_value, ...}.
    """
    file = file.split('/')[-1]
    file_entities = {}
    entity_list = file.split('_')
    for info in entity_list[:-1]:
        file_entities[info.split('-')[0]] = info.split('-')[1]
    return file_entities


def get_metadata_files_from_directory(path, suffix, extension):
    """ Get the list of metadata files in the directory

    This function is a recursive function that is used to get the list of metadata files in the directory and its subdirectories.

    :param path: str
        The path of the directory
    :param suffix: str
        The suffix of the metadata files
    :param extension: str
        The type of the metadata files
    :return: list
        The list of metadata files in the directory
    """
    files = []
    for p in os.listdir(path):
        abspath = os.path.join(path, p)
        if os.path.isdir(abspath):
            files.extend(get_metadata_files_from_directory(abspath, suffix, extension))
        else:
            if search(suffix + extension, p):
                files.append(abspath.replace('\\', '/'))
    return files


def clean_frame(frame):
    """ This function is used to clean a frame.

    This function is used to clean a frame by destroying all its children.

    :param frame: tk.Frame
        The frame to clean
    """

    for widget in frame.winfo_children():
        widget.destroy()


def contain_entities(metadata, file):
    """ Check if the metadata entities are all comprised in the file

    This function is used to check if the metadata entities are all comprised in the file
    It returns False if the file does not contain all the entities
    It returns False if the entities are not the same in the file and the metadata
    It returns True otherwise

    :param metadata: dict
        The metadata entities
    :param file: str
        The path of the file
    :return: bool
        True if the metadata entities are all comprised in the file, False otherwise
    """
    if len(file.split('/')) > 1:
        file = file.split('/')[-1]

    entities = get_entities_single_file(metadata)
    file_entities = get_entities_single_file(file)
    for entity in entities:
        if entity not in file_entities:
            return False
        else:
            if entities[entity] != file_entities[entity]:
                return False
    return True


def order_files(file_list, file):
    """ Order the files by the distance between the file and the files in the list

    This function is used to order the files by the distance between the file and the files in the list
    The first file in the list is the closest to the file and the last
    The distance is calculated by the directory hierarchy

    :param file_list: list
        The list of files to order
    :param file: BIDSFile
        The file to compare to
    :return: list
        The ordered list of files
    """

    valid_files = []

    # Remove the file that are below the file in the hierarchy
    # (if the bids project is valid it should not be necessary)
    for f in file_list:
        if f.split('/').__len__() <= file.split('/').__len__():
            valid_files.append(f)

    valid_files.sort(key=lambda x: x.split('/').__len__())
    valid_files.reverse()
    return valid_files


def process_directory(parent, path, tree):
    """ A recursive method to populate the treeview with the project directory.

    It populates the treeview with the project directory and adds the subjects and sessions as branches.
    It calls itself for each directory in the project directory.

    :param tree:
    :param parent: str
        The parent item in the treeview.
    :param path: str
        The path of the directory to process.
    """

    for p in os.listdir(path):
        abspath = os.path.join(path, p)
        isdir = os.path.isdir(abspath)

        if search('^sub-*', p) and isdir:
            oid = tree.insert(parent, 'end', text=p, open=False, tags='sub', )
        elif search('^ses-*', p) and isdir:
            oid = tree.insert(parent, 'end', text=p, open=False, tags='ses')
        else:
            oid = tree.insert(parent, 'end', text=p, open=False)
        if isdir:
            process_directory(oid, abspath, tree)


def search_inheritance(path, directory, suffix, extension='.tsv'):
    """ Search in the hierarchy for files that can be applied to the file and have the suffix

    This method is used to search in the hierarchy for files that can be applied to the file and have the suffix
    The files are ordered by the distance between the file and the files in the list
    It follows the BIDS inheritance rules so only the files that are above the file in the hierarchy are kept
    Only the first file is applicable following the BIDS inheritance rules for non-json files

    :param directory: str
        The directory of the project
    :param path: str
        The path of the file
    :param suffix: str
        The suffix of the file
    :param extension: str (default='.tsv')
        The type of the file
    :return: list
        The list of files that can be applied to the file and have the suffix
    """

    meta_list = get_metadata_files_from_directory(directory, suffix, extension)
    valid_files = []
    # Keep only the files which entities are all comprised in the file entities
    for meta in meta_list:
        if contain_entities(meta, path):
            valid_files.append(meta)
    valid_files = order_files(valid_files, path)
    return valid_files


def get_type_from_extension(extension):
    """ Get the type of the file from its extension

    This function is used to get the type of the file from its extension

    :param extension: str
        The extension of the file
    :return: str
        The type of the file
    """
    extension_dict = json.load(open(os.path.join(os.path.dirname(__file__), 'CreationTool/res/ExtensionToType.json').replace('\\', '/')))
    for key, value in extension_dict.items():
        if key == extension:
            return value
    return 'other'


def get_metadata_info_from_type(type):
    """ Get the type of the file from its extension

    This function is used to get the type of the file from its extension

    :param type: str
        The extension of the file
    :return: dict
        The dict of the metadata files for a file with the given type with their required entities
    """
    extension_dict = json.load(open(os.path.join(os.path.dirname(__file__), 'CreationTool/res/ModalityFiles.json').replace('\\', '/')))
    if type in extension_dict.keys():
        return extension_dict[type]
    return ['other']


def get_possible_metadata_files(type):
    """ Get the possible metadata files for a file with the given type

    This function is used to get the possible metadata files for a file with the given type

    :param type: str
        The type of the file
    :return: list
        The possible metadata files for a file with the given type
    """
    requirement = json.load(open(os.path.join(os.path.dirname(__file__), 'CreationTool/res/ModalityFiles.json').replace('\\', '/')))
    return list(requirement[type].keys())


def get_possible_types():
    """ Get the possible types of files

    This function is used to get the possible types of files

    :return: list
        The possible types of files
    """
    requirement = json.load(open(os.path.join(os.path.dirname(__file__), 'CreationTool/res/ModalityFiles.json').replace('\\', '/')))
    return list(requirement.keys())


def verify_inheritance(metadata_file, project_directory, start_directory, desired_impacted_files, type):
    """ Test the inheritance of the metadata file

    Verify that only the desired impacted files are impacted by the metadata file by searching the inheritance
    for every file starting from the start directory and its subdirectories.
    And verify that the metadata file has a valid name.
    Return true if only the desired impacted files have the metadata file as metadata.

    :param type:
    :param metadata_file: str
    Path to the metadata file to test for.
    :param project_directory: str
    Path to the project directory.
    :param start_directory: str
    Path to the directory to start the search from.
    :param desired_impacted_files: list
    Paths list of the desired impacted files.
    :return: bool
    True if only the desired impacted files have the metadata file as metadata.
    """
    impacted_files = []
    suffix = metadata_file.split('/')[-1].split('_')[-1].split('.')[0]
    extension = metadata_file.split('/')[-1].split('.')[-1]

    requirement = json.load(open(os.path.join(os.path.dirname(__file__), 'CreationTool/res/ModalityFiles.json').replace('\\', '/')))
    for requirement in requirement[type][suffix+'.'+extension]:
        if requirement not in metadata_file:
            return False

    extension = '.' + extension
    for root, dirs, files in os.walk(start_directory):
        for file in files:
            if not file.endswith('.tsv') and not file.endswith('.json'):
                file_path = os.path.join(root, file).replace('\\', '/')
                actual_metadata_files = search_inheritance(file_path, project_directory, suffix, extension)
                if len(actual_metadata_files) > 0 and actual_metadata_files[0] == metadata_file:
                    impacted_files.append(file_path)
    for file in desired_impacted_files:
        if file not in impacted_files:
            return False
        else:
            impacted_files.remove(file)
    if impacted_files:
        return False
    return True


def find_shared_entities(paths):
    """ This method is used to find the shared entities of the experiments.

        Return a string containing only the entities that are shared by all the experiments.
        If the task is not shared return an empty string. (The BIDS inheritance rule seem to need the task to be valid)
        :param paths: list
        A list containing the paths of the experiments.
        :return: str
        A string containing the shared entities of the experiments in the form entity-(entity_value)_.
    """
    shared_entities = {}
    for path in paths:
        entities = get_entities_single_file(path)
        for entity, value in entities.items():
            if entity in shared_entities:
                if shared_entities[entity] != value:
                    shared_entities[entity] = 'not_shared'
            else:
                shared_entities[entity] = value
    shared_entities_string = ''
    if 'task' not in shared_entities or shared_entities['task'] == 'not_shared':
        return ''
    for entity, value in shared_entities.items():
        if value != 'not_shared':
            shared_entities_string = shared_entities_string + entity + '-' + value + '_'

    return shared_entities_string


class Tools:
    """ This class is used to provide tools to the BIDS viewer package.

    Attributes:
        layout: BIDSLayout
            The BIDS layout of the project

    Methods:
        pybids_get_metadata_from_path():
            Get the metadata of a file from its path using pybids
        pybids_path():
            Get the pybids bids file from a path
        search_inheritance():
            Search in the hierarchy for files that can be applied to the file and have the suffix
    """

    def __init__(self, layout):
        """ The constructor of the class, it instantiates the tools of the package.

        :param layout: BIDSLayout
            The BIDS layout of the project for which the tools are used
        """
        self.layout = layout

    def pybids_get_metadata_from_path(self, path):
        """ Get the metadata of a file from its path using pybids

        This method is used to get the metadata of a file from its path using pybids

        :param path: str
            The path of the file
        :return: str
            The metadata of the file in json format
        """

        if platform.system() == 'Windows':
            layout_path = path.replace('/', '\\')
        else:
            layout_path = path
        metadata = self.layout.get()[self.layout.get(return_type='filename').index(layout_path)].get_metadata()
        metadata = json.dumps(metadata,
                              skipkeys=True,
                              allow_nan=True,
                              indent=6)
        return metadata

    def pybids_path(self, path):
        """ Get the pybids bids file from a path

        This method is used to get the pybids bids file from a path

        :param path: str
            The path of the file
        :return: BIDSFile
            The pybids bids file
        """

        if platform.system() == 'Windows':
            layout_path = path.replace('/', '\\')
        else:
            layout_path = path
        return self.layout.get()[self.layout.get(return_type='filename').index(layout_path)]


