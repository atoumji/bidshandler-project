import tkinter as tk
import webbrowser

import BIDSHandler.CreationTool.Application as CreationTool
import BIDSHandler.ViewingTool.Application as ViewingTool


class Application(tk.Frame):
    """ This class is the main class of the BIDS viewer package. It is used to create the main window of the application.

    Attributes:
        root: tk.Tk
            The main window of the application.
        main_frame: tk.Frame
            The main frame of the application.

    Methods:
        __init__():
            The constructor of the class, it instantiates the main window of the application.
        show():
            This method is used to show the main window of the application.
        create_widgets():
            This method is used to create the widgets of the main window of the application.
    """

    def __init__(self, root):
        """ The constructor of the class, it instantiates the main window of the application.
        """
        tk.Frame.__init__(self, root)
        root.title('BIDS Handler')
        self.root = root
        self.create_widgets()

    def create_widgets(self):
        """ This method is used to create the widgets of the main window of the application.
        """
        self.main_frame = tk.Frame(self.root)
        self.main_frame.pack(anchor='center', padx=10, pady=10, fill='both', expand=True)

        widget_frame = tk.Frame(self.main_frame)
        widget_frame.place(relx=0.5, rely=0.5, anchor='center')

        viewing_tool_button = tk.Button(widget_frame, text='Viewing Tool', command=self.viewing_tool)
        viewing_tool_button.pack(pady=10)

        creation_tool_button = tk.Button(widget_frame, text='Creation Tool', command=self.creation_tool)
        creation_tool_button.pack(pady=10)

        validation_button = tk.Button(widget_frame, text='Validate BIDS', command=self.validate_bids)
        validation_button.pack(pady=10)

        viewing_tool_button.config(height=3, width=20, relief='solid')
        creation_tool_button.config(height=3, width=20, relief='solid')
        validation_button.config(height=3, width=20, relief='solid')

    def viewing_tool(self):
        """ This method is used to show the viewing tool.
        """
        self.main_frame.destroy()

        viewing_tool = ViewingTool.Application(self.root)
        viewing_tool.pack()

    def creation_tool(self):
        """ This method is used to show the creation tool.
        """
        self.main_frame.destroy()

        creation_tool = CreationTool.Application(self.root)
        creation_tool.pack()

    def validate_bids(self):
        """ This method is used to open a web browser at https://bids-standard.github.io/bids-validator/ to validate a BIDS project.
        """
        webbrowser.open('https://bids-standard.github.io/bids-validator/')