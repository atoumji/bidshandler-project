import os
import unittest
import BIDSHandler.Tools as Tools


# To Do : replace os.path.join(os.path.dirname(__file__), 'res/directory') by self.project_directory


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.project_directory = os.path.join(os.path.dirname(__file__), 'res', 'directory').replace('\\', '/')

    def test_get_entities(self):
        result = {'sub-01': {'ses-2': [
            os.path.join(os.path.dirname(__file__),
                         'res/directory/sub-01/ses-2/eeg/sub-01_ses-2_task-travail_run-01_eeg.edf').replace('\\',
                                                                                                            '/')]}}
        experiment_file = [os.path.join(self.project_directory, 'sub-01', 'ses-2', 'eeg',
                                        'sub-01_ses-2_task-travail_run-01_eeg.edf').replace('\\', '/')]
        self.assertEqual(result, Tools.get_entities(experiment_file, self.project_directory))

        result = {'sub-01': {'ses-2': [
            os.path.join(os.path.dirname(__file__),
                         'res/directory/sub-01/ses-2/eeg/sub-01_ses-2_task-travail_run-01_eeg.edf').replace('\\',
                                                                                                            '/')], },
            'sub-02': {'ses-1': [
                os.path.join(os.path.dirname(__file__),
                             'res/directory/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_eeg.edf').replace('\\',
                                                                                                                '/')]}}
        experiment_file.append(os.path.join(self.project_directory, 'sub-02', 'ses-1', 'eeg',
                                            'sub-02_ses-1_task-travail_run-01_eeg.edf').replace('\\', '/'))
        self.assertEqual(result, Tools.get_entities(experiment_file, self.project_directory))

    def test_get_entities_single_file(self):
        result = {'sub': '01', 'ses': '2', 'task': 'travail', 'run': '01'}
        experiment_file = [
            os.path.join(self.project_directory, 'sub-01_ses-2_task-travail_run-01_eeg.edf').replace('\\', '/')]
        self.assertEqual(result, Tools.get_entities_single_file(experiment_file[0]))

        result = {'sub': '02', 'ses': '1', 'task': 'travail', 'run': '01'}
        experiment_file = [
            os.path.join(self.project_directory, 'sub-02_ses-1_task-travail_run-01_eeg.edf').replace('\\', '/')]
        self.assertEqual(result, Tools.get_entities_single_file(experiment_file[0]))

    def test_get_metadata_files_from_directory(self):
        result = [
            os.path.join(os.path.dirname(__file__), 'res/directory/ses-1_task-travail_channels.tsv').replace('\\', '/'),
            os.path.join(os.path.dirname(__file__),
                         'res/directory/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_channels.tsv').replace('\\',
                                                                                                                 '/')]
        self.assertEqual(result, Tools.get_metadata_files_from_directory(
            os.path.join(os.path.dirname(__file__), 'res/directory/').replace('\\', '/'), 'channels', '.tsv'))

        result = [result[1]]
        self.assertEqual(result, Tools.get_metadata_files_from_directory(
            os.path.join(os.path.dirname(__file__), 'res/directory/sub-02').replace('\\', '/'), 'channels', '.tsv'))

    def test_contain_entities(self):
        metadata = 'sub-01_ses-2_task-travail_run-01_channels.tsv'
        file = 'sub-01_ses-2_task-travail_run-01_eeg.edf'
        self.assertTrue(Tools.contain_entities(metadata, file))

        file = 'sub-01_ses-2_task-travail_run-02_eeg.edf'
        self.assertFalse(Tools.contain_entities(metadata, file))

        metadata = 'sub-01_ses-2_task-travail_channels.tsv'
        self.assertTrue(Tools.contain_entities(metadata, file))

        file = 'sub-01_task-travail_run-02_eeg.edf'
        self.assertFalse(Tools.contain_entities(metadata, file))

    def test_order_files(self):
        files = [self.project_directory + '/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_channels.tsv',
                 self.project_directory + '/ses-1_task-travail_channels.tsv']
        result = files

        self.assertEqual(result, Tools.order_files(files,
                                                   self.project_directory + '/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_eeg.edf'))

        files = [files[1], files[0]]

        self.assertEqual(result, Tools.order_files(files,
                                                   self.project_directory + '/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_eeg.edf'))

    def test_search_inheritance(self):
        metadata_file = [self.project_directory + '/ses-1_task-travail_channels.tsv']
        file = self.project_directory + '/sub-03/ses-1/eeg/sub-03_ses-1_task-travail_run-02_eeg.edf'
        self.assertEqual(metadata_file, Tools.search_inheritance(file, self.project_directory, 'channels'))

        metadata_file = [self.project_directory + '/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_channels.tsv',
                         self.project_directory + '/ses-1_task-travail_channels.tsv']
        file = self.project_directory + '/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_eeg.edf'
        self.assertEqual(metadata_file, Tools.search_inheritance(file, self.project_directory, 'channels'))

        metadata_file = [self.project_directory + '/sub-01/ses-2/eeg/sub-01_ses-2_task-bonjour_run-01_events.tsv']
        file = self.project_directory + '/sub-01/ses-2/eeg/sub-01_ses-2_task-bonjour_run-01_eeg.edf'
        self.assertEqual(metadata_file, Tools.search_inheritance(file, self.project_directory, 'events'))

    def test_get_type_from_extension(self):
        self.assertEqual('eeg', Tools.get_type_from_extension('edf'))

    def test_get_metadata_info_from_type(self):
        result = {'channels.json': ['task'],
                  'channels.tsv': ['task'],
                  'coordsystem.json': [],
                  'eeg.json': ['task'],
                  'electrodes.json': [],
                  'electrodes.tsv': [],
                  'events.json': ['task'],
                  'events.tsv': ['task'],
                  'physio.json': ['task'],
                  'physio.tsv.gz': ['task'],
                  'stim.json': ['task'],
                  'stim.tsv.gz': ['task']}
        self.assertEqual(result, Tools.get_metadata_info_from_type('eeg'))

    def test_get_possible_metadata_files(self):
        result = ['channels.tsv', 'channels.json', 'eeg.json', 'events.tsv', 'events.json', 'coordsystem.json',
                  'electrodes.tsv', 'electrodes.json', 'physio.json', 'physio.tsv.gz', 'stim.json', 'stim.tsv.gz']
        self.assertEqual(result, Tools.get_possible_metadata_files('eeg'))

    def test_get_possible_types(self):
        result = ['eeg']
        self.assertEqual(result, Tools.get_possible_types())

    def test_verify_inheritance(self):
        metadata_file = self.project_directory + '/ses-1_task-travail_channels.tsv'
        desired_impacted_files = [
            self.project_directory + '/sub-03/ses-1/eeg/sub-03_ses-1_task-travail_run-02_eeg.edf']

        self.assertTrue(
            Tools.verify_inheritance(metadata_file, self.project_directory, self.project_directory,
                                     desired_impacted_files,
                                     'eeg'))

    def test_find_shared_entities(self):
        files = [self.project_directory + '/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_eeg.edf',
                 self.project_directory + '/sub-01/ses-2/eeg/sub-01_ses-2_task-travail_run-01_eeg.edf']
        self.assertEqual('task-travail_run-01_',
                         Tools.find_shared_entities(files))

        files = [self.project_directory + '/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_eeg.edf',
                 self.project_directory + '/sub-01/ses-2/eeg/sub-01_ses-2_task-bonjour_run-02_eeg.edf']
        self.assertEqual('', Tools.find_shared_entities(files))


if __name__ == '__main__':
    unittest.main()
