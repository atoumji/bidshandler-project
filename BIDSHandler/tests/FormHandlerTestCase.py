import os
import unittest
from BIDSHandler.CreationTool.FormHandler import FormHandler
import tkinter as tk
import pandas as pd


class FormHandlerTestCase(unittest.TestCase):
    def setUp(self):
        self.frame = tk.Toplevel()
        necessary_forms = ["dataset_description.json", "participants.tsv"]
        # Initialize a FormHandler instance here
        # You might need to adjust this to match your actual setup
        self.form_handler = FormHandler(self.frame, necessary_forms, '../CreationTool/res/Requirements.json')

    def test_show_forms(self):
        # Test that the form at index 0 is shown
        self.form_handler.show_forms(0)
        self.assertEqual({}, self.form_handler.form_frame_list[0].frame.grid_info())

        # Test that the form at index 1 is not shown
        self.assertFalse(self.form_handler.form_frame_list[1].frame.grid_info())

    def test_is_valid_mandatory(self):
        # This test open a notification window, so it is not suitable for automated testing
        # To Do: Find a way to test this without opening a window
        path = os.path.join(os.path.dirname(__file__), "res", "test_dataset_description.json")
        index = 0

        self.assertFalse(
            self.form_handler.form_frame_list[index].is_valid_mandatory()
        )

        self.form_handler.import_data(index, path)

        self.assertTrue(
            self.form_handler.form_frame_list[index].is_valid_mandatory()
        )

        path = os.path.join(os.path.dirname(__file__), "res", "test_participants.tsv")
        false_path = os.path.join(os.path.dirname(__file__), "res", "test_false_participants.tsv")
        index = 1

        # Actually a tsv is considered valid if it is empty, this might be subject to change in the future
        self.assertTrue(
            self.form_handler.form_frame_list[index].is_valid_mandatory()
        )

        self.form_handler.import_data(index, false_path)

        self.assertFalse(
            self.form_handler.form_frame_list[index].is_valid_mandatory()
        )

        self.form_handler.import_data(index, path)

        self.assertTrue(
            self.form_handler.form_frame_list[index].is_valid_mandatory()
        )

    def test_import_data(self):
        path = os.path.join(os.path.dirname(__file__), "res", "test_participants.tsv")
        index = 1
        self.form_handler.import_data(index, path)

        expected_df = pd.read_csv(path, sep="\t")
        self.assertTrue(expected_df.equals(self.form_handler.form_frame_list[index].table.model.df))

    def test_save_forms(self):
        path = os.path.join(os.path.dirname(__file__), "res", "test_participants.tsv")
        result_path = os.path.join(os.path.dirname(__file__), "res", "results")
        result_file_path = os.path.join(result_path, "participants.tsv")
        index = 1
        self.form_handler.import_data(index, path)
        self.form_handler.save_forms(1, result_path)

        expected_df = pd.read_csv(path, sep="\t")
        result_df = pd.read_csv(result_file_path, sep="\t")
        self.assertTrue(expected_df.equals(result_df))


if __name__ == '__main__':
    unittest.main()
