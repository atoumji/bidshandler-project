import os
import unittest
import BIDSHandler.CreationTool.Application as Application
import tkinter as tk


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.app = Application.Application(root=tk.Toplevel())
        self.app.project_directory = os.path.join(os.path.dirname(__file__), 'res', 'directory').replace('\\', '/')
        self.app.number_of_subjects = 3
        self.app.number_of_sessions = 2
        self.app.experiment_type = tk.StringVar()
        self.app.experiment_type.set('eeg')
        self.app.sidecar_type = tk.StringVar()
        self.app.sidecar_type.set('channels.tsv')

    def test_get_optimal_files(self):
        result = [os.path.join(os.path.dirname(__file__), 'res/directory/sub-01/ses-2/eeg/sub-01_ses-2_task-travail_run-01_channels.tsv').replace('\\', '/')]
        experiment_file = [os.path.join(self.app.project_directory, 'sub-01', 'ses-2', 'eeg', 'sub-01_ses-2_task-travail_run-01_eeg.edf').replace('\\', '/')]
        self.assertEqual(result, self.app.get_optimal_files(experiment_file, 'channels.tsv'))

        experiment_file.append(os.path.join(self.app.project_directory, 'sub-02', 'ses-1', 'eeg', 'sub-02_ses-1_task-travail_run-01_eeg.edf').replace('\\', '/'))
        result = [os.path.join(os.path.dirname(__file__), 'res/directory/sub-01/ses-2/eeg/sub-01_ses-2_task-travail_run-01_channels.tsv').replace('\\', '/'),
                  os.path.join(os.path.dirname(__file__), 'res/directory/sub-02/ses-1/eeg/sub-02_ses-1_task-travail_run-01_channels.tsv').replace('\\', '/')]
        self.assertEqual(result, self.app.get_optimal_files(experiment_file, 'channels.tsv'))


if __name__ == '__main__':
    unittest.main()
