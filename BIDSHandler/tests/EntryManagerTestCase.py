import unittest
from BIDSHandler.CreationTool.EntryManager import create_entry_from_json, text_to_condition
import tkinter as tk
from PIL import Image, ImageTk
import os


class EntryManagerTestCase(unittest.TestCase):
    def setUp(self):
        self.root = tk.Toplevel()
        self.frame = tk.Frame(self.root)
        self.frame.pack()
        self.form = ("dataset_description.json", {
            "Name": {
                "requirement level": "required",
                "multiple": False,
                "description": "Name of the dataset",
                "valid condition": "len(str(value)) > 0"
            }
        }
                     )
        with Image.open(os.path.join(os.path.dirname(__file__), '../CreationTool/res/help_icon.png')) as img_help:
            img_help = img_help.resize((20, 20))
            self.help_icon = ImageTk.PhotoImage(img_help)

        self.entries = create_entry_from_json(self.form, self.frame, self.help_icon)

    def test_create_entry_from_json(self):

        self.assertEqual(len(self.entries), 1)
        self.assertEqual('Name', self.entries[0].get_name())
        self.assertEqual('', self.entries[0].get_text())

    def test_text_to_condition(self):
        self.assertTrue(text_to_condition('value > 0', 1))
        self.assertFalse(text_to_condition('value > 0', -1))

    def test_check_valid(self):
        self.assertFalse(self.entries[0].check_valid())

        self.entries[0].set_text('test')
        self.assertTrue(self.entries[0].check_valid())

        self.entries[0].set_text('')
        self.assertFalse(self.entries[0].check_valid())


if __name__ == '__main__':
    unittest.main()
