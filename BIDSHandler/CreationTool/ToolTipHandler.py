from tkinter import *


class ToolTip(object):
    """
        The ToolTip class is used to create a tooltip for a widget in the BIDS Handler creation tool.

        Attributes:
            widget (tkinter.Widget): The widget for which the tooltip is created.
            tipwindow (tkinter.Toplevel): The window in which the tooltip is displayed.
            id (int): The id of the tooltip.
            x (int): The x-coordinate of the tooltip.
            y (int): The y-coordinate of the tooltip.
            text (str): The text displayed in the tooltip.

        Methods:
            __init__(widget: tkinter.Widget): Initializes the ToolTip with the given widget.
            showtip(text: str): Displays the given text in the tooltip window.
            hidetip(): Hides the tooltip window.
        """

    def __init__(self, widget):
        """The constructor of the class, it creates a tooltip for the given widget.

        :param widget: tkinter.Widget
            The widget for which the tooltip is created.
        """
        self.widget = widget
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0

    def showtip(self, text):
        """This method is used to display the given text in the tooltip window.

        :param text: str
            The text to be displayed in the tooltip window.
        """
        self.text = ""
        for c in text:
            self.text += c
            if len(self.text)+2 <= len(text) and c == '.' and self.text[-2] != '.' and text[len(self.text) + 2] != '.':
                self.text += "\n"

        if self.tipwindow or not self.text:
            return
        x, y, cx, cy = self.widget.bbox("insert")
        x = x + self.widget.winfo_rootx() + 57
        y = y + cy + self.widget.winfo_rooty() + 27
        self.tipwindow = tw = Toplevel(self.widget)
        tw.wm_overrideredirect(1)
        tw.wm_geometry("+%d+%d" % (x, y))
        label = Label(tw, text=self.text, justify=LEFT,
                      background="#ffffe0", relief=SOLID, borderwidth=1,
                      font=("tahoma", "8", "normal"))
        label.pack(ipadx=1)

    def hidetip(self):
        """This method is used to hide the tooltip window."""
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()


def create_tool_tip(widget, text):
    """ This function creates a tooltip for the given widget and displays the given text when the mouse hovers over the widget.

    :param widget: tkinter.Widget
        The widget for which the tooltip is created.
    :param text: str
        The text to be displayed in the tooltip.
    """
    tool_tip = ToolTip(widget)

    def enter(event):
        tool_tip.showtip(text)

    def leave(event):
        tool_tip.hidetip()
    widget.bind('<Enter>', enter)
    widget.bind('<Leave>', leave)
