import tkinter as tk
from ctypes import windll
from ctypes.wintypes import DWORD
from tkinter import ttk
# Necessary for the text_to_condition function
import re
from BIDSHandler.CreationTool.ToolTipHandler import create_tool_tip


def text_to_condition(string, value):
    """ This function is used to evaluate a condition.

    :param string: str
        The condition to evaluate
    :param value: any
        The value to evaluate
    :return: bool
        True if the condition is satisfied, False otherwise
    """
    result = "(" + string + ")"
    result = result.replace(';', 'and')
    try:
        return eval(result)
    except (ValueError, SyntaxError):
        return False


def create_entry_from_json(form, frame, help_icon):
    """ This function is used to create an entry widget from a json form.

    :param form: tuple
        A tuple containing the name of the form and the form itself
    :param frame: tk.Frame
        The frame in which the entry widget is placed
    :param help_icon: tk.PhotoImage
        The icon of the help button
    :return: list
        A list containing the entry widgets
    """
    name_style = {'required': 'bold', 'recommended': 'normal', 'optional': 'italic'}
    current_row = 1
    entries = []
    if 'linked_file' in form[1]:
        return
    for name, info in form[1].items():
        if 'modulable' in info:
            if info['modulable']:
                label = tk.Entry(frame)
                label.grid(row=current_row, column=0)
            else:
                label = tk.Label(frame, text=name, font=('Arial', 12, name_style[info['requirement level']]))
                label.grid(row=current_row, column=0)
        else:
            label = tk.Label(frame, text=name, font=('Arial', 12, name_style[info['requirement level']]))
            label.grid(row=current_row, column=0)
        help_label = tk.Label(frame, image=help_icon)
        help_label.grid(row=current_row, column=2)
        create_tool_tip(help_label, info['description'])
        if 'type' in info:
            data = info['type']
            data['label_name'] = name
            button = tk.Button(frame, text='Fill information', command=lambda: entries.append(SubEntry(frame, data, label, current_row, help_icon)))
            button.grid(row=current_row, column=1)
        elif info['multiple']:
            entries.append(MultipleEntry(frame, label, current_row, 1, 2, info['description'], info['valid condition'], info['requirement level']))
        else:
            entries.append(Entry(frame, label, current_row, 1, 1, info['valid condition'], info['requirement level']))
        current_row += 1
    return entries


class Entry:
    """ This class is used to create an entry widget.

    The entry widget is a widget that allows the user to enter text.

    Attributes:
        frame: tk.Frame
            The frame in which the entry widget is placed
        valid_condition: str
            The condition that the entry widget must satisfy
        row: int
            The row in which the entry widget is placed
        column: int
            The column in which the entry widget is placed
        columnspan: int
            The columnspan of the entry widget
        state: str
            The state of the entry widget
        multiple: bool
            True if the entry widget is a multiple entry, False otherwise
        text: tk.StringVar
            The text of the entry widget
        label: tk.Label
            The label of the entry widget (the name of the entry widget)
        entry: tk.Entry
            The entry widget

    Methods:
        update_color():
            This method is used to update the color of the entry widget.
        check_valid():
            This method is used to check if the entry widget is valid.
        update_state():
            This method is used to update the state of the entry widget.
        get_text():
            This method is used to get the text of the entry widget.
        get_name():
            This method is used to get the name of the entry widget.
        set_text(text):
            This method is used to set the text of the entry widget.
    """

    def __init__(self, frame, label, row, column, columnspan, valid_condition, requirement, state='invalid'):
        """ The constructor of the class, it instantiates the entry widget.

        :param frame: tk.Frame
            The frame in which the entry widget is placed
        :param label: tk.Label
            The label of the entry widget (the name of the entry widget)
        :param row: int
            The row in which the entry widget is placed
        :param column: int
            The column in which the entry widget is placed
        :param columnspan: int
            The columnspan of the entry widget
        :param valid_condition: str
            The condition that the entry widget must satisfy
        :param requirement: str
            The requirement level of the entry widget
        :param state: str (default='invalid')
            The state of the entry widget
        """
        self.frame = frame
        self.valid_condition = valid_condition
        self.row = row
        self.column = column
        self.columnspan = columnspan
        self.state = state
        self.multiple = False
        self.text = tk.StringVar()
        self.label = label
        self.text.trace('w', self.update_color)
        self.entry = tk.Entry(self.frame, textvariable=self.text)
        self.entry.grid(row=self.row, column=self.column, columnspan=self.columnspan)
        self.entry.bind('<KeyRelease>', lambda event: self.update_state())
        if requirement != 'required':
            self.state = 'not required'
        self.update_color()

    def update_color(self, *args):
        """ This method is used to update the color of the entry widget.

        The color of the entry widget is updated based on the state of the entry.
        """
        if self.state == 'invalid':
            # The background color of the entry widget is red with a 0.5 opacity if the entry widget is invalid
            self.entry.config({'background': 'red'})
        elif self.state == 'not required':
            self.entry.config({'background': 'white'})
        else:
            self.entry.config({'background': '#00FF00'})

    def check_valid(self):
        """ This method is used to check if the entry widget is valid.

        :return: bool
            True if the entry widget meet its valid condition, False otherwise
        """
        if self.valid_condition == '':
            return True
        return text_to_condition(self.valid_condition, self.entry.get())

    def update_state(self):
        """ This method is used to update the state of the entry widget.

        The state of the entry widget is updated based on the validity of the entry widget.
        """
        if self.check_valid():
            self.state = 'valid'
        else:
            self.state = 'invalid'
        self.update_color()
        self.frame.winfo_toplevel().event_generate('<<EntryStateChanged>>')

    def get_text(self):
        """ This method is used to get the text of the entry widget.

        :return: str
            The text of the entry widget
        """
        return self.entry.get()

    def get_name(self):
        """ This method is used to get the name of the entry widget.

        :return: str
            The name of the entry widget
        """
        if isinstance(self.label, tk.Label):
            return self.label.cget('text')
        else:
            return self.label.get()

    def set_text(self, text):
        """ This method is used to set the text of the entry widget.

        :param text: str
            The text of the entry widget
        """
        self.text.set(text)
        self.update_state()


class MultipleEntry:
    """ This class is used to create a multiple entry widget.

    The multiple entry widget is a widget that allows the user to enter multiple texts.

    Attributes:
        frame: tk.Frame
            The frame in which the entry widget is placed
        label: tk.Label
            The label of the entry widget (the name of the entry widget)
        row: int
            The row in which the entry widget is placed
        column: int
            The column in which the entry widget is placed
        columnspan: int
            The columnspan of the entry widget
        entries: list
            A list containing the entry widgets
        state: str
            The state of the entry widget
        add_button: tk.Button
            The button to add more entries

    Methods:
        add_entry():
            This method is used to add an entry to the frame.
        get_text():
            This method is used to get the text of the entry widget.
        get_name():
            This method is used to get the name of the entry widget.
    """

    def __init__(self, frame, label, row, column, columnspan, description, valid_condition, requirement, state='invalid'):
        """ This method is used to create a frame with an entry and a button to add more entries.

        :param frame: tk.Frame
            The frame in which the entry widget is placed
        :param label: tk.Label
            The label of the entry widget (the name of the entry widget)
        :param row: int
            The row in which the entry widget is placed
        :param column: int
            The column in which the entry widget is placed
        :param columnspan: int
            The columnspan of the entry widget
        :param description: str
            The description of the entry widget
        :param valid_condition: str
            The condition that the entry widget must satisfy
        :param requirement: str
            The requirement level of the entry widget
        :param state: str (default='invalid')
            The state of the entry widget
        """

        self.valid_condition = valid_condition
        self.frame = tk.Frame(frame)
        self.frame.grid(row=row, column=column, columnspan=columnspan)
        self.label = label
        self.row = 0
        self.column = 0
        self.columnspan = columnspan
        self.entries = []
        self.requirement = requirement
        self.state = state
        self.entries.append(Entry(self.frame, self.label, self.row, self.column, self.columnspan-1, self.valid_condition, self.requirement, state))
        self.add_button = tk.Button(self.frame, text='+', command=self.add_entry)
        self.add_button.grid(row=self.row, column=self.columnspan)

        create_tool_tip(self.add_button, description)

    def add_entry(self):
        """ This method is used to add an entry to the frame.
        """
        self.row += 1
        self.entries.append(Entry(self.frame, self.label, self.row, self.column, self.columnspan-1, self.valid_condition, self.requirement))
        self.add_button.grid(row=self.row, column=self.columnspan)

    def get_text(self):
        """ This method is used to get the text of the entry widget.

        :return: str
            The text of the entry widget
        """
        data = []
        for entry in self.entries:
            data.append(entry.get_text())
        return data

    def get_name(self):
        """ This method is used to get the name of the entry widget.

        :return: str
            The name of the entry widget
        """
        if isinstance(self.label, tk.Label):
            return self.label.cget('text')
        else:
            return self.label.get()


class SubEntry:
    """ This class is used to create a sub entry widget.

    The sub entry widget is a widget that allows the user to enter multiple texts.
    This class is recursive, a sub entry can have sub entries.

    Attributes:
        name_style: dict
            A dict containing the style of the name of the sub entry widget
        current_row: int
            The current row of the sub entry widget
        sub_entry_dict: dict
            A dict containing the sub entry widgets
        state: str
            The state of the sub entry widget
        name: str
            The name of the sub entry widget
        return_dict: dict
            A dict containing the text of the sub entry widget
        label: tk.Label
            The label of the sub entry widget (the name of the sub entry widget)
        sub_entry_dict: dict
            A dict containing the sub entry widgets
        current_row: int
            The current row of the sub entry widget

    Methods:
        save():
            This method is used to save the text of the sub entry widget.
        get_text():
            This method is used to get the text of the sub entry widget.
        get_name():
            This method is used to get the name of the sub entry widget.
    """

    def __init__(self, frame, form, label, row, help_icon, state='valid'):
        """ The constructor of the class, it instantiates a sub entry widget.

        The sub entry has a button that opens a new window with more entries.
        This class is recursive, a sub entry can have sub entries.

        :param frame: tk.Frame
            The frame in which the sub entry widget is placed
        :param form: dict
            A dict containing the information of the sub entry widget
        :param label: tk.Label
            The label of the sub entry widget (the name of the sub entry widget)
        :param row: int
            The row in which the sub entry widget is placed
        :param help_icon: tk.PhotoImage
            The icon of the help button
        :param state: str (default='invalid')
            The state of the sub entry widget
        """
        self.name_style = {'required': 'bold', 'recommended': 'normal', 'optional': 'italic'}
        self.current_row = row
        self.sub_entry_dict = {}
        self.state = state
        self.name = form['label_name']
        form.pop('label_name')
        self.return_dict = {}
        self.label = label
        sub_window = tk.Toplevel(frame)
        sub_frame = tk.Frame(sub_window)
        sub_frame.pack()
        current_row = 1
        for name, info in form.items():
            if 'modulable' in info:
                if info['modulable']:
                    label = tk.Entry(sub_frame)
                    label.grid(row=current_row, column=0)
                else:
                    label = tk.Label(sub_frame, text=name, font=('Arial', 12, self.name_style[info['requirement level']]))
                    label.grid(row=current_row, column=0)
            else:
                label = tk.Label(sub_frame, text=name, font=('Arial', 12, self.name_style[info['requirement level']]))
                label.grid(row=current_row, column=0)
            if 'type' in info:
                data = info['type']
                if isinstance(label, tk.Entry):
                    label_name = label.get()
                else:
                    label_name = label.cget('text')
                data['label_name'] = label_name
                button = tk.Button(sub_frame, text='Fill information', command=lambda: self.sub_entry_dict.update({name: SubEntry(sub_frame, data, label, current_row, help_icon)}))
                button.grid(row=current_row, column=1)
            elif info['multiple']:
                self.sub_entry_dict[name] = MultipleEntry(sub_frame, label, current_row, 1, 1, info['description'], info['valid condition'], info['requirement level'])
            else:
                self.sub_entry_dict[name] = Entry(sub_frame, label, current_row, 1, 1, info['valid condition'], info['requirement level'])

            current_row += 1

            # The save button leaves the sub window
        save_button = tk.Button(sub_frame, text='Save', command=lambda: self.save())
        save_button.grid(row=current_row, column=0, columnspan=3)

    def save(self):
        """ This method is used to save the text of the sub entry widget.
        """
        for entry in self.sub_entry_dict.values():
            if isinstance(entry.label, tk.Label):
                name = entry.label.cget('text')
            else:
                name = entry.label.get()
            self.return_dict[name] = entry.get_text()
            if self.return_dict[name] == '':
                del self.return_dict[name]
        return self.return_dict

    def get_text(self):
        """ This method is used to get the text of the sub entry widget.

        :return: str
            The text of the sub entry widget
        """
        return self.return_dict

    def get_name(self):
        """ This method is used to get the name of the sub entry widget.

        :return: str
            The name of the sub entry widget
        """
        if self.name == '':
            return self.label.get()
        else:
            return self.name
