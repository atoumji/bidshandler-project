import json
import os
import shutil
import tkinter as tk
import platform
from pathlib import Path
from tkinter import ttk, messagebox

import BIDSHandler.MainApplication as MainApplication
import BIDSHandler.Tools as Tools
from BIDSHandler.CreationTool.EntryManager import Entry
from BIDSHandler.CreationTool.FormHandler import FormHandler


class Application(tk.Frame):
    """ This class is the main class of the BIDS Handler creation tool. It is used to create the main window of the application.

    Attributes:
        root: tk.Tk
            The main window of the application.
        main_frame: tk.Frame
            The main frame of the application.

    Methods:
        __init__():
            The constructor of the class, it instantiates the main window of the application.
        show():
            This method is used to show the main window of the application.
        create_widgets():
            This method is used to create the widgets of the main window of the application.
    """

    def __init__(self, root):
        tk.Frame.__init__(self, root)
        self.root = root
        self.menu_with_main_frame = tk.Frame(self.root)
        self.menu_with_main_frame.pack(anchor='center', fill='both', expand=True)
        self.main_frame = tk.Frame(self.menu_with_main_frame)

        self.possible_forms = json.load(open(os.path.join(os.path.dirname(__file__), 'res/BaseFile.json')))

        self.actual_step = 0

        self.create_widgets()

    def create_widgets(self):
        """ This method is used to create the widgets of the main window of the application.

        It creates a button to start a new BIDS project, a button to complete a BIDS project
        and a button to go back to the tool selection.
        """

        self.menu_frame = tk.Frame(self.menu_with_main_frame)
        self.menu_frame.pack(anchor='nw')

        self.file_bar = ttk.Menubutton(self.menu_frame, text='Menu')
        self.file_bar.pack(anchor='nw', side='left')

        self.help_bar = ttk.Menubutton(self.menu_frame, text='Help')
        self.help_bar.pack(anchor='nw', side='left')


        self.file_menu = tk.Menu(self.file_bar, tearoff=0)
        self.file_menu.add_command(label='Tool Selection', command=self.tool_selection)
        self.file_menu.add_command(label='Quit', command=self.root.quit)

        self.help_menu = tk.Menu(self.help_bar, tearoff=0)
        self.help_menu.add_command(label='Help', command=self.display_help)

        self.file_bar['menu'] = self.file_menu
        self.help_bar['menu'] = self.help_menu

        self.main_frame.pack(anchor='center', padx=10, pady=10, fill='both', expand=True)

        center_frame = tk.Frame(self.main_frame)
        center_frame.pack(anchor='center', padx=10, pady=10, fill='both', expand=True)

        widget_frame = tk.Frame(center_frame)
        widget_frame.place(relx=0.5, rely=0.5, anchor='center')

        start_bids_project_button = tk.Button(widget_frame, text='Start BIDS Project',
                                              command=self.start_bids_project)
        start_bids_project_button.grid(row=0, column=0, pady=10, padx=10)

        complete_bids_project_button = tk.Button(widget_frame, text='Complete BIDS Project',
                                                 command=self.complete_bids_project)
        complete_bids_project_button.grid(row=0, column=1)

        tool_selection_button = tk.Button(widget_frame, text='Back to Tool Selection', command=self.tool_selection)
        tool_selection_button.grid(row=1, column=0, columnspan=2)

        start_bids_project_button.config(height=3, width=20, relief='solid')
        complete_bids_project_button.config(height=3, width=20, relief='solid')
        tool_selection_button.config(height=3, width=20, relief='solid')


    def display_help(self):
        """ This method is used to display the help window of the application.

        The help window contains information about the application and how to use it.
        """


        current_help = json.load(open(os.path.join(os.path.dirname(__file__), 'res/Help.json')))
        current_help = current_help[str(self.actual_step)]

        messagebox.showinfo('Help', current_help)

    def start_bids_project(self):
        """ This method is used to start the creation process of a new BIDS project.

            The user is asked to provide basic information about the new BIDS project.
            Like the name of the project, the directory of the project and the files he wants to include.
        """
        Tools.clean_frame(self.main_frame)

        self.middle_frame = tk.Frame(self.main_frame)
        self.middle_frame.pack(anchor='center', padx=10, pady=10, fill='both', expand=True)

        self.center_frame = tk.Frame(self.middle_frame)
        self.center_frame.place(relx=0.5, rely=0.5, anchor='center')
        self.project_directory = None

        name_label = tk.Label(self.center_frame, text='Name of the project:')
        name_label.grid(row=0, column=0)
        self.name_entry = Entry(self.center_frame, label=name_label, row=0, column=1, columnspan=1,
                                valid_condition='len(value) > 0', requirement='required')

        path_button = tk.Button(self.center_frame, text='Select project directory',
                                command=self.select_project_directory)
        path_button.grid(row=2, column=0, columnspan=2)

        self.confirm_frame = tk.Frame(self.main_frame)
        self.confirm_frame.pack(anchor='s', padx=10, pady=10)

        self.confirm_button = tk.Button(self.confirm_frame, text='Confirm', command=self.form_creation,
                                        state='disabled')
        self.confirm_button.grid(row=1, column=0, columnspan=2)
        self.confirm_button.config(height=3, width=20, relief='solid')

        checkbox_frame = tk.Frame(self.center_frame)
        checkbox_frame.grid(row=1, column=0, columnspan=2)
        checkbox_frame_label = tk.Label(checkbox_frame, text='Select the information you want to include')
        checkbox_frame_label.pack()

        self.checkbox_vars = []

        for form, requirement in self.possible_forms.items():
            self.checkbox_vars.append(tk.IntVar())
            checkbox = tk.Checkbutton(checkbox_frame, text=form, variable=self.checkbox_vars[-1])
            if requirement == 'required':
                checkbox.select()
                checkbox.config(state='disabled')
            checkbox.pack()

        self.center_frame.winfo_toplevel().bind('<<EntryStateChanged>>',
                                                lambda *_: self.confirm_button.config(state='normal')
                                                if self.name_entry.check_valid() and self.project_directory is not None
                                                else self.confirm_button.config('disabled'))

    def select_project_directory(self):
        """ This method is used to select the directory of the new BIDS project.

            The user is prompted to select the directory of the new BIDS project.
            After the directory is selected, the user is shown the path of the directory and the confirm button is enabled.
        """
        if self.project_directory is not None:
            self.project_directory_label.pack_forget()
            self.project_directory_label.destroy()
        self.project_directory = tk.filedialog.askdirectory()

        if self.project_directory == '':
            messagebox.showerror('Error', 'No directory selected\nPlease select a directory')
            return
        self.project_directory_label = tk.Label(self.confirm_frame, text='Project directory: ' + self.project_directory)
        self.project_directory_label.grid(row=0, column=0, columnspan=2)
        self.center_frame.winfo_toplevel().event_generate('<<EntryStateChanged>>')

    def form_creation(self):
        """ This method is used to create the first form of the new BIDS project (Usually the dataset description).

            The user is shown a form in which to fill with information of the dataset description.
            The confirm button is disabled until the form is_valid_mandatory is True.
        """

        self.actual_step += 1
        self.selected_forms = []

        for i, var in enumerate(self.checkbox_vars):
            if var.get() != 0:
                self.selected_forms.append(list(self.possible_forms.keys())[i])

        self.project_name = self.name_entry.get_text()

        self.project_directory = os.path.join(self.project_directory, self.project_name)
        self.project_directory = self.project_directory.replace('\\', '/')

        self.create_directory()
        self.middle_frame.destroy()

        self.confirm_frame.pack_forget()

        self.center_frame = tk.Frame(self.main_frame)
        self.center_frame.pack(anchor='center', padx=10, pady=10, fill='y', expand=True)
        self.confirm_frame.pack(anchor='s', padx=10, pady=10)



        self.form_handler = FormHandler(self.center_frame, self.selected_forms, 'res/Requirements.json')
        self.form_handler.show_forms(0)
        self.confirm_button.config(text='Create', command=self.form_finish, state='disabled')

        self.center_frame.winfo_toplevel().bind('<<EntryStateChanged>>',
                                                lambda *_: self.check_mandatory(self.form_handler.form_frame_list[0],
                                                                                self.confirm_button))

    def form_finish(self):
        """ This method is used to finish the creation of the current form.

        The form is saved and the user is prompted to continue to the next form.
        """
        self.form_handler.save_forms(0, self.project_directory)
        self.form_handler.form_frame_list[0].frame.grid_forget()
        self.form_handler.form_frame_list[0].frame.destroy()
        self.form_handler.form_frame_list.pop(0)
        self.form_handler.show_forms(0)
        self.confirm_button.config(text='Confirm', command=self.next_step, state='disabled')

        if '.md' in self.form_handler.form_frame_list[0].form[0] or '.' not in \
                self.form_handler.form_frame_list[0].form[0]:
            self.confirm_button.config(state='normal')

        self.form_handler.frame.winfo_toplevel().bind('<<EntryStateChanged>>', lambda *_: self.check_mandatory(
            self.form_handler.form_frame_list[0], self.confirm_button))

    def next_step(self):
        """ This method is used to go to the next step of the creation of the new BIDS project.

            Depending on the actual step, this method return to the next step of the creation of the new BIDS project.
            If all the forms are filled, return to the architecture creation.
            If the architecture is created, return to the data integration.
            If the data is integrated, return to the sidecar integration.
        """
        if self.actual_step == 1:
            if not self.form_handler.form_frame_list.__len__() <= 1:
                self.form_finish()
            else:
                self.actual_step += 1
                self.form_handler.save_forms(0, self.project_directory)
                self.form_handler.form_frame_list[0].frame.grid_forget()
                self.form_handler.form_frame_list[0].frame.destroy()
                self.form_handler.form_frame_list.pop(0)
                self.architecture_creation()
        elif self.actual_step == 2:
            self.actual_step += 1
            self.integrate_data()
        elif self.actual_step == 3:
            self.actual_step += 1
            self.integrate_sidecar()

    def check_mandatory(self, form, validation_button):
        """ This method is used to check if the form is valid.

            The confirm button is enabled if the form is valid.

            :param form: Form
                The form to check.
            :param validation_button: Button
                The button to enable or disable.
        """
        if form.is_valid_mandatory():
            validation_button.config(state='normal')
        else:
            validation_button.config(state='disabled')

    def create_directory(self):
        """ This method is used to create the directory of the new BIDS project.

            The directory of the new BIDS project is created.
            If the directory already exists, the user is prompted to overwrite it.
        """
        if os.path.exists(self.project_directory):
            overwrite = tk.messagebox.askyesno('Overwrite',
                                               'The directory already exists, do you want to overwrite it?')
            if overwrite:
                # Remove the directory even if it is not empty.
                import shutil
                shutil.rmtree(self.project_directory)
                os.mkdir(self.project_directory)
            else:
                self.project_directory = tk.filedialog.askdirectory()
                self.project_directory = os.path.join(self.project_directory, self.project_name).replace('\\', '/')
                self.create_directory()
        else:
            os.mkdir(self.project_directory)

    def complete_bids_project(self):
        """ To be implemented.
        """
        messagebox.showinfo('Information', 'This feature is not implemented yet')

    def tool_selection(self):
        """ This method is used to go back to the tool selection.

            The user is guided back to the tool selection.
        """

        self.menu_with_main_frame.destroy()
        main_application = MainApplication.Application(self.root)
        main_application.pack()
        main_application.mainloop()

    def architecture_creation(self):
        """ This method is used to start the creation of the architecture of the new BIDS project.

            The user is asked to provide the number of sessions of the new BIDS project.
        """

        Tools.clean_frame(self.main_frame)

        self.architecture_frame = tk.Frame(self.main_frame)
        self.architecture_frame.place(relx=0.5, rely=0.5, anchor='center')

        self.architecture_label = tk.Label(self.architecture_frame, text='Architecture of the project')
        self.architecture_label.grid(row=0, column=0)
        self.architecture_label.config(font=('Arial', 20))

        self.session_label = tk.Label(self.architecture_frame, text='Number of sessions:')
        self.session_label.grid(row=1, column=0)
        self.session_entry = tk.Entry(self.architecture_frame)
        self.session_entry.grid(row=1, column=1)

        self.architecture_confirm_button = tk.Button(self.architecture_frame, text='Confirm',
                                                     command=self.architecture_finish)
        self.architecture_confirm_button.grid(row=2, column=0, columnspan=2)

        self.architecture_confirm_button.config(height=3, width=20, relief='solid')

    def architecture_finish(self):
        """ This method is used to finish the creation of the architecture of the new BIDS project.

            Following the number of sessions provided, and the information filled in the participant.tsv file,
            the architecture of the new BIDS project is created.
            If only one session is provided, the architecture will not have sessions subdirectories.
        """
        self.number_of_sessions = int(self.session_entry.get())
        Tools.clean_frame(self.architecture_frame)
        self.architecture_label = tk.Label(self.architecture_frame, text='Creating architecture of the project...')
        self.architecture_label.pack()

        with open(os.path.join(self.project_directory, 'participants.tsv'), 'r') as file:
            participants = file.read()

        participants = participants.split('\n')
        participants.pop(0)

        self.number_of_subjects = 0

        for i in participants:
            i = i.split('\t')
            if i[0] == '':
                continue
            path = os.path.join(self.project_directory, i[0])
            os.mkdir(path)
            self.number_of_subjects += 1
            if self.number_of_sessions > 1:
                for j in range(1, self.number_of_sessions + 1):
                    os.mkdir(os.path.join(path, 'ses-' + str(j)))

        self.architecture_label.config(text='Architecture of the project created')
        self.architecture_show_project = tk.Button(self.architecture_frame, text='Show project state',
                                                   command=self.actual_project_state)
        self.architecture_show_project.pack()

        self.architecture_confirm_button = tk.Button(self.architecture_frame, text='Continue', command=self.next_step)
        self.architecture_confirm_button.pack()

        self.architecture_show_project.config(height=3, width=20, relief='solid')
        self.architecture_confirm_button.config(height=3, width=20, relief='solid')

    def actual_project_state(self):
        """ This method is used to show the current state of the project.

            A new window is created to show the current state of the project in a treeview.
        """

        self.project_state_window = tk.Toplevel(self.root)
        self.project_state_window.title('Project state')

        self.project_state_label = tk.Label(self.project_state_window, text='Project state')
        self.project_state_label.pack()

        self.project_state_tree = ttk.Treeview(self.project_state_window)
        self.project_state_tree.pack(anchor='center', expand=True, fill='both')

        abspath = os.path.abspath(self.project_directory)
        root_node = self.project_state_tree.insert('', 'end', text=os.path.basename(self.project_directory), open=True)
        Tools.process_directory(root_node, abspath, self.project_state_tree)

    def integrate_data(self):
        """ This method is used to integrate the data of the new BIDS project.

            The main window for the data integration step.
            The user is shown the buttons to integrate data, show the project state and continue to the next step.
        """
        Tools.clean_frame(self.main_frame)

        self.integrate_data_frame = tk.Frame(self.main_frame)
        self.integrate_data_frame.place(relx=0.5, rely=0.5, anchor='center')

        integrate_data_label = tk.Label(self.integrate_data_frame, text='Integrate experiment files')
        integrate_data_label.pack()
        integrate_data_label.config(font=('Arial', 20))

        self.integrate_data_button = tk.Button(self.integrate_data_frame, text='Integrate data',
                                               command=self.integrate_data_file)
        self.integrate_data_button.pack()

        self.show_project_button = tk.Button(self.integrate_data_frame, text='Show project state',
                                             command=self.actual_project_state)
        self.show_project_button.pack()

        self.integrate_data_confirm_button = tk.Button(self.integrate_data_frame, text='Continue',
                                                       command=self.next_step)
        self.integrate_data_confirm_button.pack()

        self.integrate_data_button.config(height=3, width=20, relief='solid')
        self.show_project_button.config(height=3, width=20, relief='solid')
        self.integrate_data_confirm_button.config(height=3, width=20, relief='solid')

        self.previous_directory = None

    def integrate_data_file(self):
        """ This method is used to integrate a file to the new BIDS project.

            A new window is created to guide the user through the process of integrating a file to the new BIDS project.
            The user is first prompted to select the file to integrate.
        """
        self.integrate_data_window = tk.Toplevel(self.root)
        self.integrate_data_window.title('Integrate data')

        self.integrate_data_label = tk.Label(self.integrate_data_window, text='Integrate data')
        self.integrate_data_label.grid(row=0, column=0)

        self.integrate_data_button = tk.Button(self.integrate_data_window, text='Select file',
                                               command=self.select_experiment_file)
        self.integrate_data_button.grid(row=1, column=0)

    def select_experiment_file(self):
        """ This method is used to select the file to integrate to the new BIDS project.

            The user is prompted to select the file to integrate to the new BIDS project.
            The confirm button is enabled after the file is selected.
        """
        if self.previous_directory is not None:
            self.experiment_file = tk.filedialog.askopenfilename(initialdir=self.previous_directory)
        else:
            self.experiment_file = tk.filedialog.askopenfilename()
        self.previous_directory = os.path.dirname(self.experiment_file)
        self.integrate_data_window.lift()
        self.integrate_data_label.config(text='File selected: ' + self.experiment_file)

        # Select the subject and session for the file by selecting the directory.
        self.integrate_data_button.config(text='Select subject/session', command=self.select_directory)

    def select_directory(self):
        """ This method is used to select the directory to integrate the file to.

            The user is prompted to choose the subject and session to integrate the file to.
            It is done by selecting the directory of the subject and session.
            The user is then prompted to fill the task, acquisition and run of the file.
        """

        # The directory can only be in the project directory.
        self.directory = tk.filedialog.askdirectory(initialdir=self.project_directory)
        self.integrate_data_window.lift()

        self.integrate_data_label.config(
            text='File selected: ' + self.experiment_file + '\nDirectory selected: ' + self.directory)
        self.integrate_data_button.grid_forget()

        # To Do : make this method generic by using a json file
        font = tk.font.nametofont("TkDefaultFont").actual().copy()
        font["weight"] = "bold"
        self.task_label = tk.Label(self.integrate_data_window, text='Task:', font=font)
        self.task_label.grid(row=1, column=0)
        self.task_entry = Entry(self.integrate_data_window, label=self.task_label, row=1, column=1, columnspan=1,
                                valid_condition='len(value) > 0', requirement='required')

        font["weight"] = "normal"
        self.acquisition_label = tk.Label(self.integrate_data_window, text='Acquisition:', font=font)
        self.acquisition_label.grid(row=2, column=0)
        self.acquisition_entry = Entry(self.integrate_data_window, label=self.acquisition_label, row=2, column=1,
                                       columnspan=1, valid_condition='len(value) > 0', requirement='recommended')

        self.run_label = tk.Label(self.integrate_data_window, text='Run:', font=font)
        self.run_label.grid(row=3, column=0)
        self.run_entry = Entry(self.integrate_data_window, label=self.run_label, row=3, column=1, columnspan=1,
                               valid_condition='len(value) > 0', requirement='recommended')

        self.integrate_data_button = tk.Button(self.integrate_data_window, text='Integrate',
                                               command=self.integrate_data_finish, state='disabled')
        self.integrate_data_button.grid(row=4, column=0, columnspan=2)

        self.integrate_data_window.bind('<<EntryStateChanged>>', lambda *_: self.check_mandatory_integrate_data())

    def integrate_data_finish(self):
        """ This method is used to finish the integration of the file to the new BIDS project.

            The file is integrated to the new BIDS project.
            It is saved in the selected directory with the selected task, acquisition and run.
            It is named following the BIDS standard.
        """
        data = {'task': self.task_entry.get_text(), 'acq': self.acquisition_entry.get_text(),
                'run': self.run_entry.get_text()}

        Tools.clean_frame(self.integrate_data_window)

        status_label = tk.Label(self.integrate_data_window, text='Integrating data...')
        status_label.pack()

        leave_button = tk.Button(self.integrate_data_window, text='Leave', command=self.integrate_data_window.destroy)
        leave_button.pack()

        # Get the name of the file.
        # Get the path from the project directory to the file.
        directory = self.directory.split(self.project_directory)[1].split('/')
        filename = ""
        for info in directory[1:]:
            filename = filename + info + '_'

        for name, value in data.items():
            if value != '':
                filename = filename + name + '-' + value + '_'
        extension = self.experiment_file.split('.')[-1]
        filename = filename + Tools.get_type_from_extension(extension) + '.' + extension

        self.directory = os.path.join(self.directory, Tools.get_type_from_extension(extension))
        if not os.path.exists(self.directory):
            os.mkdir(self.directory)
        self.directory = self.directory.replace('\\', '/')
        shutil.copy2(self.experiment_file, self.directory + '/' + filename)

        status_label.config(text='Data integrated')

    def check_mandatory_integrate_data(self):
        """ This method is used to check if the form is valid.

            The confirm button is enabled if the form is valid.
        """
        if self.task_entry.get_text() != '':
            self.integrate_data_button.config(state='normal')
        else:
            self.integrate_data_button.config(state='disabled')

    def integrate_sidecar(self):
        """ This method is used to integrate the sidecar of the new BIDS project.

            The main window for the sidecar integration step.
            The user is shown five buttons :
            - Integrate sidecar (Integrate an already existing sidecar to the new BIDS project)
            - Integrate and modify sidecar (Modify before integrating an already existing sidecar to the new BIDS project)
            - Create sidecar (Create a new sidecar for the new BIDS project)
            - Show project state (Show the current state of the project)
            - Open project (Open the directory of the project as the project is considered finished once the sidecars are integrated)
        """
        Tools.clean_frame(self.main_frame)
        self.integrate_and_modify = False

        self.integrate_sidecar_frame = tk.Frame(self.main_frame)
        self.integrate_sidecar_frame.place(relx=0.5, rely=0.5, anchor='center')

        integrate_sidecar_label = tk.Label(self.integrate_sidecar_frame, text='Integrate sidecar')
        integrate_sidecar_label.pack()
        integrate_sidecar_label.config(font=('Arial', 20))

        self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_frame, text='Integrate sidecar',
                                                  command=self.integrate_sidecar_file)
        self.integrate_sidecar_button.pack()

        self.integrate_and_modify_button = tk.Button(self.integrate_sidecar_frame, text='Integrate and modify',
                                                     command=self.integrate_and_modify_sidecar)
        self.integrate_and_modify_button.pack()

        self.create_sidecar_button = tk.Button(self.integrate_sidecar_frame, text='Create sidecar',
                                               command=self.create_sidecar)
        self.create_sidecar_button.pack()

        self.show_project_button = tk.Button(self.integrate_sidecar_frame, text='Show project state',
                                             command=self.actual_project_state)
        self.show_project_button.pack()

        if platform.system() == 'Windows':
            open_project_button = tk.Button(self.integrate_sidecar_frame, text='Open project',
                                        command=lambda: os.startfile(self.project_directory))
        else:
            open_project_button = tk.Button(self.integrate_sidecar_frame, text='Open project',
                                        command=lambda: os.system('xdg-open ' + self.project_directory))
        open_project_button.pack()

        self.integrate_sidecar_button.config(height=3, width=20, relief='solid')
        self.integrate_and_modify_button.config(height=3, width=20, relief='solid')
        self.create_sidecar_button.config(height=3, width=20, relief='solid')
        self.show_project_button.config(height=3, width=20, relief='solid')
        open_project_button.config(height=3, width=20, relief='solid')

        self.previous_directory = None

    def integrate_sidecar_file(self):
        """ This method is used to integrate a sidecar to the new BIDS project.

            A new window is created to guide the user through the process of integrating a sidecar to the new BIDS project.
            He is first prompted to select the file to integrate.
        """
        self.integrate_and_modify = False
        self.integrate_sidecar_window = tk.Toplevel(self.root)
        self.integrate_sidecar_window.title('Integrate sidecar')

        self.integrate_sidecar_label = tk.Label(self.integrate_sidecar_window, text='Integrate sidecar')
        self.integrate_sidecar_label.grid(row=0, column=0)

        self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Select file',
                                                  command=self.select_sidecar_file)
        self.integrate_sidecar_button.grid(row=1, column=0)

    def integrate_and_modify_sidecar(self):
        """ This method is used to start the integration and modify a sidecar to the new BIDS project.

            The user is guided through the process of integrating and modifying a sidecar to the new BIDS project.
            A variable is set to True to indicate that the user wants to modify the sidecar before integrating it.
            The user is then redirected to the creation of the sidecar.
        """
        self.integrate_and_modify = True
        self.create_sidecar()

    def create_sidecar(self):
        """ This method is used to create a sidecar of the new BIDS project.

            A new window is created to guide the user through the process of creating a sidecar of the new BIDS project.
            He is first prompted to select the type of the sidecar (e.g. channels.tsv, events.tsv, etc.).
        """
        self.integrate_sidecar_window = tk.Toplevel(self.root)
        self.integrate_sidecar_window.title('Create sidecar')

        self.integrate_sidecar_label = tk.Label(self.integrate_sidecar_window, text='Create sidecar')
        self.integrate_sidecar_label.grid(row=0, column=0)

        self.sidecar_type_label = tk.Label(self.integrate_sidecar_window,
                                           text='Select the type of experiment the sidecar will be applied to:')
        self.sidecar_type_label.grid(row=1, column=0)

        self.experiment_type = tk.StringVar()
        self.experiment_type.set('eeg')
        self.experiment_type_dropdown = ttk.Combobox(self.integrate_sidecar_window, textvariable=self.experiment_type)
        self.experiment_type_dropdown['values'] = Tools.get_possible_types()
        self.experiment_type_dropdown.grid(row=1, column=1)
        self.experiment_type_dropdown.config(state='readonly')

        self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Continue',
                                                  command=self.create_sidecar_second)
        self.integrate_sidecar_button.grid(row=2, column=0)

    def create_sidecar_second(self):
        """ This method is used to continue the creation of the sidecar of the new BIDS project.

            The user is guided through the process of continuing the creation of the sidecar of the new BIDS project.
            He is prompted to select the type of the sidecar (e.g. channels, events, etc.).
        """
        self.integrate_sidecar_window.lift()
        self.integrate_sidecar_label.config(text='Create sidecar\nType selected: ' + self.experiment_type.get())

        self.experiment_type_dropdown.destroy()

        self.sidecar_type_label = tk.Label(self.integrate_sidecar_window, text='Select the type of the sidecar:')
        self.sidecar_type_label.grid(row=1, column=0)

        self.sidecar_type = tk.StringVar()
        self.sidecar_type.set(Tools.get_possible_metadata_files(self.experiment_type.get())[0])
        self.sidecar_type_dropdown = ttk.Combobox(self.integrate_sidecar_window, textvariable=self.sidecar_type)
        self.sidecar_type_dropdown['values'] = Tools.get_possible_metadata_files(self.experiment_type.get())
        self.sidecar_type_dropdown.grid(row=1, column=1)
        self.sidecar_type_dropdown.config(state='readonly')

        if self.integrate_and_modify:
            self.integrate_sidecar_button.config(text='Select sidecar',
                                                 command=self.integrate_and_modify_select_sidecar)
        else:
            self.integrate_sidecar_button.config(text='Continue', command=self.create_sidecar_third)

    def select_sidecar_type(self):
        """ This method is used to select the type of the sidecar to create.

            The user is guided through the process of selecting the type of the sidecar to create.
        """
        self.integrate_sidecar_window.lift()
        self.integrate_sidecar_label.config(text='Create sidecar\nType selected: ' + self.sidecar_type.get())

        self.change_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Change type',
                                               command=self.create_sidecar)

        self.integrate_sidecar_button.config(text='Create', command=self.create_sidecar_second, state='normal')

    def integrate_and_modify_select_sidecar(self):
        """ This method is used to select the sidecar to integrate and modify.

            The user is guided through the process of selecting the sidecar to integrate and modify.
            The user is asked to select the file to integrate and modify.
        """
        self.sidecar_type_label.destroy()
        self.sidecar_type_dropdown.destroy()
        if self.previous_directory is not None:
            self.sidecar_file = tk.filedialog.askopenfilename(initialdir=self.previous_directory)
        else:
            self.sidecar_file = tk.filedialog.askopenfilename()
        self.previous_directory = os.path.dirname(self.sidecar_file)
        self.sidecar_file = self.sidecar_file.replace('\\', '/')
        self.integrate_sidecar_window.lift()
        self.integrate_sidecar_label.config(text='File selected: ' + self.sidecar_file)

        self.integrate_sidecar_button.config(text='Continue', command=self.create_sidecar_third)

    def create_sidecar_third(self):
        """ This method is used to finish the creation of the sidecar of the new BIDS project.

            Following the type selected, the user is presented with a form to fill that will be saved as the sidecar.
            For .json files, the form is a dictionary.
            For .tsv files, the form is a table.
            If the user selected to integrate and modify the sidecar, the form is filled with the data of the selected file
            before being presented to the user.
        """
        self.integrate_sidecar_window.destroy()
        self.integrate_sidecar_window = tk.Toplevel(self.root)
        self.integrate_sidecar_window.title('Create sidecar')

        self.integrate_sidecar_label = tk.Label(self.integrate_sidecar_window, text='Creating sidecar...')
        self.integrate_sidecar_label.grid(row=0, column=0)

        self.form_handler = FormHandler(self.integrate_sidecar_window, [self.sidecar_type.get()], 'res/Sidecars.json')

        if self.integrate_and_modify:
            self.form_handler.import_data(0, self.sidecar_file)

        self.form_handler.show_forms(0)

        self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Confirm',
                                                  command=self.create_sidecar_finish, state='disabled')
        self.integrate_sidecar_button.grid(row=2, column=0)

        self.form_handler.frame.winfo_toplevel().bind('<<EntryStateChanged>>', lambda *_: self.check_mandatory(
            self.form_handler.form_frame_list[0], self.integrate_sidecar_button))

    def create_sidecar_finish(self):
        """ This method is used to finish the creation of the sidecar of the new BIDS project.

            The sidecar is temporarily saved in the res folder of the application.
            The user is then prompted to select the experiments to apply the sidecar to.
        """
        self.sidecar_file = self.project_directory + '/' + 'res/' + self.sidecar_type.get()
        os.mkdir(self.project_directory + '/' + 'res')
        self.form_handler.save_forms(0, self.project_directory + '/' + 'res')
        self.form_handler.form_frame_list[0].frame.grid_forget()
        self.form_handler.form_frame_list[0].frame.destroy()
        self.form_handler.form_frame_list.pop(0)
        self.experiments_files = []

        self.integrate_sidecar_label.config(text='Sidecar created')
        self.integrate_sidecar_button.config(text='Select experiments', command=lambda: self.select_experiments(True),
                                             state='normal')

    def select_sidecar_file(self):
        """ This method is used to select the file to integrate to the new BIDS project.

            The user is guided through the process of selecting the file to integrate to the new BIDS project.
            The user is also shown a dropdown menu to select the type of the sidecar (e.g. channels, events, etc.).
            The option are loaded from a json file.
        """
        if self.previous_directory is not None:
            self.sidecar_file = tk.filedialog.askopenfilename(initialdir=self.previous_directory)
        else:
            self.sidecar_file = tk.filedialog.askopenfilename()
        self.previous_directory = os.path.dirname(self.sidecar_file)
        self.integrate_sidecar_window.lift()
        self.integrate_sidecar_label.config(text='File selected: ' + self.sidecar_file)

        self.dropdown_label = tk.Label(self.integrate_sidecar_window,
                                       text='Select the type of the experiment files the sidecar will be applied to:')
        self.dropdown_label.grid(row=2, column=0)

        self.experiment_type = tk.StringVar()
        self.experiment_type.set('eeg')
        self.sidecar_type_dropdown = ttk.Combobox(self.integrate_sidecar_window, textvariable=self.experiment_type)
        self.sidecar_type_dropdown['values'] = list(json.load(
            open(os.path.join(os.path.dirname(__file__), 'res/ModalityFiles.json'))).keys())
        self.sidecar_type_dropdown.grid(row=2, column=1)
        self.sidecar_type_dropdown.config(state='readonly')

        self.integrate_sidecar_button.config(text='Continue', command=self.select_sidecar_file_second, state='normal')
        self.integrate_sidecar_button.grid(row=3, column=0)

    def select_sidecar_file_second(self):
        """ This method is used to finish the selection of the file to integrate to the new BIDS project.

            The user is guided through the process of finishing the selection of the file to integrate to the new BIDS project.
            The user is also prompted to select the experiments to apply the sidecar to.
        """
        self.integrate_sidecar_label.config(
            text='File selected: ' + self.sidecar_file + '\nType: ' + self.experiment_type.get())

        self.dropdown_label.config(text='Select the type of the sidecar:')

        self.sidecar_type = tk.StringVar()
        self.sidecar_type.set(Tools.get_possible_metadata_files(self.experiment_type.get())[0])
        self.sidecar_type_dropdown = ttk.Combobox(self.integrate_sidecar_window, textvariable=self.sidecar_type)
        self.sidecar_type_dropdown['values'] = Tools.get_possible_metadata_files(self.experiment_type.get())
        self.sidecar_type_dropdown.grid(row=2, column=1)
        self.sidecar_type_dropdown.config(state='readonly')

        self.integrate_sidecar_button.config(text='Select experiments', command=self.select_experiments)
        self.integrate_sidecar_button.grid(row=3, column=0)
        self.experiments_files = []

    def select_experiments(self, create=False):
        """ This method is used to select the experiments to apply the sidecar to.

            The user is guided through the process of selecting the experiments to apply the sidecar to.

            :param create: bool (default=False)
                A boolean to indicate if the user selected the integrate and modify option.
        """

        Tools.clean_frame(self.integrate_sidecar_window)
        self.integrate_sidecar_window.lift()
        self.integrate_sidecar_label = tk.Label(self.integrate_sidecar_window,
                                                text='File selected: ' + self.sidecar_file +
                                                     '\nType: ' + self.sidecar_type.get() +
                                                     '\nExperiments selected: ' + str(
                                                      self.experiments_files))
        self.integrate_sidecar_label.grid(row=0, column=0)

        # To Do : add buttons to select all the experiments of a subject or a session etc.
        button_frame = tk.Frame(self.integrate_sidecar_window)
        button_frame.grid(row=1, column=0)
        self.add_experiment_button = tk.Button(button_frame, text='Add experiment',
                                               command=lambda: self.sub_select_experiments(True, create))
        self.add_experiment_button.grid(row=0, column=0)

        self.add_directory_button = tk.Button(button_frame, text='Add directory',
                                              command=lambda: self.sub_select_experiments(False, create))
        self.add_directory_button.grid(row=0, column=1)

        self.modify_button = tk.Button(self.integrate_sidecar_window, text='Modify selection',
                                        command=lambda: self.modify_experiment_selection(create))
        self.modify_button.grid(row=2, column=0)

        if len(self.experiments_files) > 0:
            if not create:
                self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Integrate', command=self.integrate_sidecar_finish,
                                                          state='normal')
            else:
                self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Integrate', command=self.place_created_sidecar,
                                                          state='normal')
        else:
            self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Integrate', state='disabled')
        self.integrate_sidecar_button.grid(row=3, column=0)

    def sub_select_experiments(self, file, create):
        """ This method is used to select the experiments to apply the sidecar to.

            The user is prompted to select the experiments to apply the sidecar to.
            The user can select a file or a directory.
            The confirm button is enabled after the experiments are selected.

            :param file: bool
                A boolean to indicate if the user selected a file or a directory.
            :param create: bool
                A boolean to indicate if the user selected the integrate and modify or create option.
        """
        # To Do : Select only the experiments with the right extension
        if file:
            experiment = tk.filedialog.askopenfilename(initialdir=self.project_directory)
            self.experiments_files.append(experiment)
        else:
            experiment = tk.filedialog.askdirectory(initialdir=self.project_directory)
            for root, dirs, files in os.walk(experiment):
                for file in files:
                    if file.split('_')[-1].split('.')[0] == self.experiment_type.get():
                        self.experiments_files.append(os.path.join(root, file).replace('\\', '/'))
        self.select_experiments(create)

    def modify_experiment_selection(self, create):
        """ Open a window with all the currently selected experiments to modify the selection.

        Each experiment is displayed with a checkbox to remove it from the selection.
        When the user is done, he can confirm the selection.
        All the experiments without a checkbox are kept in the selection.
        """
        self.modify_window = tk.Toplevel(self.root)
        self.modify_window.title('Modify selection')

        self.modify_label = tk.Label(self.modify_window, text='Modify selection, keep only checked experiments:')
        self.modify_label.pack()

        self.checkbox_vars = []
        for exp in self.experiments_files:
            var = tk.IntVar()
            self.checkbox_vars.append(var)
            checkbox = tk.Checkbutton(self.modify_window, text=exp, variable=var)
            checkbox.select()
            checkbox.pack()

        self.modify_button = tk.Button(self.modify_window, text='Confirm', command=lambda: self.modify_finish(create))
        self.modify_button.pack()

    def modify_finish(self, create):
        """ Finish the modification of the selection.

        Remove the experiments that have been unchecked from the selection.
        """
        new_selection = []
        for i in range(len(self.experiments_files)):
            if self.checkbox_vars[i].get() == 1:
                new_selection.append(self.experiments_files[i])
        self.experiments_files = new_selection
        self.modify_window.destroy()
        self.select_experiments(create)

    def integrate_sidecar_finish(self):
        """ This method is used to finish the integration of the sidecar to the new BIDS project.

            The sidecar is integrated to the new BIDS project.
            It is saved following the bids standard on inheritance.
            It is placed at the optimal place in the hierarchy to be applied to the chosen experiments.
            It is named following the BIDS standard.
        """
        self.integrate_sidecar_window.destroy()
        self.integrate_sidecar_window = tk.Toplevel(self.root)
        self.integrate_sidecar_window.title('Integrate sidecar')

        self.integrate_sidecar_label = tk.Label(self.integrate_sidecar_window, text='Integrating sidecar...')
        self.integrate_sidecar_label.pack()

        self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Leave',
                                                  command=self.integrate_sidecar_window.destroy)
        self.integrate_sidecar_button.pack()

        path = self.get_optimal_files(self.experiments_files, self.sidecar_type.get())
        for p in path:
            shutil.copy2(self.sidecar_file, p)

        self.integrate_sidecar_label.config(text='Sidecar integrated')

    def get_optimal_files(self, experiments_files, sidecar_type):
        """ This method is used to get the optimal files to apply the sidecar to.

            The optimal files to apply the sidecar to are found.
            The sidecar is placed at the optimal place in the hierarchy to be applied only to the chosen experiments.
            :param experiments_files: list
                The list of the experiments to apply the sidecar to.
            :param sidecar_type: str
                The type of the sidecar to apply. (e.g. channels.tsv, events.tsv, etc.)
            :return: list
            A list containing the optimal emplacements of the sidecar
        """
        emplacement = Tools.get_entities(experiments_files, self.project_directory)
        path_list = []
        no_shared = False

        if emplacement.keys().__len__() == self.number_of_subjects or (
                emplacement.keys().__len__() >= self.number_of_subjects / 2 and self.number_of_subjects > 2):
            concerned_files = []
            for sub in emplacement.keys():
                for ses in emplacement[sub].keys():
                    concerned_files.extend(emplacement[sub][ses])
            path = Tools.find_shared_entities(concerned_files)
            path = self.project_directory + '/' + path + sidecar_type
            if path == '':
                no_shared = True
            else:
                Path(path).touch()
                if Tools.verify_inheritance(path, self.project_directory, self.project_directory, experiments_files,
                                            self.experiment_type.get()):
                    path_list.append(path)
                else:
                    no_shared = True
                os.remove(path)
        if no_shared or path_list.__len__() == 0:
            no_shared = False
            for sub in emplacement.keys():
                first_if = False
                if emplacement[sub].keys().__len__() == self.number_of_sessions or (emplacement[
                                                                                        sub].keys().__len__() >= self.number_of_sessions / 2 and self.number_of_sessions > 2):
                    first_if = True
                    concerned_files = []
                    for ses in emplacement[sub].keys():
                        concerned_files.extend(emplacement[sub][ses])
                    path = Tools.find_shared_entities(concerned_files)
                    path = self.project_directory + '/' + sub + '/' + path + sidecar_type
                    if path == '':
                        no_shared = True
                    else:
                        Path(path).touch()
                        if Tools.verify_inheritance(path, self.project_directory, self.project_directory + '/' + sub,
                                                    experiments_files, self.experiment_type.get()):
                            path_list.append(path)
                        else:
                            no_shared = True
                        os.remove(path)
                if no_shared or not first_if:
                    for ses in emplacement[sub].keys():
                        concerned_files = []
                        concerned_files.extend(emplacement[sub][ses])
                        path = Tools.find_shared_entities(concerned_files)
                        # To Do automate the selection of the data type folder
                        path_list.append(self.project_directory + '/' + sub + '/' + ses + '/' + Tools.get_type_from_extension(
                            concerned_files[0].split('.')[-1]) + '/' + path + sidecar_type)
        print(path_list)
        return path_list

    def place_created_sidecar(self):
        """ This method is used to place the created sidecar to the new BIDS project.

            The created sidecar is placed to the new BIDS project.
            It is saved following the bids standard on inheritance.
            It is placed at the optimal place in the hierarchy to be applied to the chosen experiments.
            It is named following the BIDS standard.
        """
        self.integrate_and_modify = False
        self.integrate_sidecar_window.destroy()
        self.integrate_sidecar_window = tk.Toplevel(self.root)
        self.integrate_sidecar_window.title('Integrate sidecar')

        self.integrate_sidecar_label = tk.Label(self.integrate_sidecar_window, text='Placing sidecar...')
        self.integrate_sidecar_label.grid(row=0, column=0)

        self.integrate_sidecar_button = tk.Button(self.integrate_sidecar_window, text='Leave',
                                                  command=self.integrate_sidecar_window.destroy)
        self.integrate_sidecar_button.grid(row=1, column=0)

        path = self.get_optimal_files(self.experiments_files, self.sidecar_type.get())

        for p in path:
            shutil.copy2(self.sidecar_file, p)

        shutil.rmtree(self.project_directory + '/' + 'res')
        self.integrate_sidecar_label.config(text='Sidecar placed')
