import json
import os
import tkinter as tk
import warnings
from tkinter import messagebox as mb

import pandas as pd
from PIL import Image, ImageTk
from pandastable import Table

import BIDSHandler.CreationTool.EntryManager as EntryManager
from BIDSHandler.CreationTool.ToolTipHandler import create_tool_tip


class Form:
    """This class is used to create a form based on a json file.

    Attributes:
         form: tuple
            The form is a tuple containing the name of the form and a dictionary containing the fields of the form.
        name_style: dict
            A dictionary containing the style of the name of the field in accordance to its requirement level.
        frame: Frame
            The frame containing the form.
        current_row: int
            The current row of the form.
        entries: list
            A list containing the entries of the form.
        help_icon: PhotoImage
            The icon used to display a help message.
        invalid_icon: PhotoImage
            The icon used to display an invalid message.
        valid_icon: PhotoImage
            The icon used to display a valid message.
        table: Table (optional if the form is a tsv file)
            The table used to display the form if it is a tsv file.
        confirmation_button: Button (optional if the form is a tsv file)
            The button used to validate the form if it is a tsv file.
        validation_label: Label (optional if the form is a tsv file)
            The label used to display the validation state of the form if it is a tsv file.

    Methods:
        create_form_json():
            This method is used to create a form for a json file.
        create_form_text():
            This method is used to create a form for a text file.
        create_form_tsv():
            This method is used to create a form for a tsv file.
        is_valid_mandatory():
            This method is used to check if the form is valid.
        save_form():
            This method is used to save the form.
        save_form_json():
            This method is used to save the form in a json file.
        save_form_md():
            This method is used to save the form in a markdown file.
        save_form_tsv():
            This method is used to save the form in a tsv file.
        notify():
            This method is used to notify the application that the state of an entry has changed.
        import_data():
            This method is used to import data in the form.
        import_data_json():
            This method is used to import data in the form from a json file.
        import_data_md():
            This method is used to import data in the form from a markdown file.
        import_data_tsv():
            This method is used to import data in the form from a tsv file.
    """

    def __init__(self, form, frame, help_icon, invalid_icon, valid_icon):
        """The constructor of the class, it creates a form based on the form given in parameter.

        :param form: tuple
            The form is a tuple containing the name of the form and a dictionary containing the fields of the form.
        :param frame: Frame
            The frame containing the form.
        :param help_icon: PhotoImage
            The icon to hover to display a help message.
        :param invalid_icon: PhotoImage
            The icon used to indicate that the form is invalid.
        :param valid_icon: PhotoImage
            The icon used to indicate that the form is valid.
        """
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.form = form
        self.name_style = {'required': 'bold', 'recommended': 'normal', 'optional': 'italic'}
        self.frame = tk.Frame(frame)
        self.frame.pack(anchor='center', padx=10, pady=10, fill='both', expand=True)
        title = tk.Label(self.frame, text=self.form[0], font=('Arial', 16, 'bold'))
        title.grid(row=0, column=0, columnspan=3)

        self.current_row = 1
        self.entries = []
        self.help_icon = help_icon
        self.invalid_icon = invalid_icon
        self.valid_icon = valid_icon
        if '.json' in self.form[0]:
            self.create_form_json()
        elif '.tsv' in self.form[0]:
            self.create_form_tsv()
        else:
            self.create_form_text()

    def create_form_json(self):
        """This method is used to create a form for a json file.
        """
        self.entry_canvas = tk.Canvas(self.frame, height=self.frame.master.master.winfo_height()*0.8)
        frame = tk.Frame(self.entry_canvas)
        scrollbar = tk.Scrollbar(self.frame, orient="vertical", command=self.entry_canvas.yview)
        self.entry_canvas.create_window((0, 0), window=frame, anchor='nw')
        self.entry_canvas.configure(yscrollcommand=scrollbar.set)
        self.entry_canvas.grid(row=self.current_row, column=0, sticky='nsew')
        scrollbar.grid(row=self.current_row, column=5, sticky='ns')
        frame.bind("<Configure>", lambda e: self.entry_canvas.configure(scrollregion=self.entry_canvas.bbox("all")))

        self.entries = EntryManager.create_entry_from_json(self.form, frame, self.help_icon)

    def create_form_text(self):
        """This method is used to create a form for a text file.
        """
        text_entry = tk.Text(self.frame, height=10, width=50)
        text_entry.grid(row=self.current_row, column=0, columnspan=5, rowspan=10)
        text_entry.config(relief='solid')

        self.current_row += 10
        help_label = tk.Label(self.frame, image=self.help_icon)
        help_label.grid(row=self.current_row, column=3)
        create_tool_tip(help_label, self.form[1]["description"])
        self.entries.append(text_entry)

    def is_valid_mandatory(self):
        """This method is used to check if the form is valid.

        :return: bool
            True if the form is valid, False otherwise.
        """
        if '.md' in self.form[0]:
            return True
        elif '.json' in self.form[0]:
            for entry in self.entries:
                if entry.state == 'invalid' and self.form[1][entry.label.cget('text')]['requirement level'] == 'required':
                    return False
            return True
        elif '.tsv' in self.form[0]:
            # Check is all required columns are present in the table.
            for column in self.form[1]:
                if self.form[1][column]['requirement level'] == 'required':
                    if column not in self.table.model.df.columns:
                        self.validation_label.config(image=self.invalid_icon)
                        mb.showinfo('Missing column',
                                    'The column ' + column + ' is missing.\n'
                                    'Please add it to the table.')
                        self.frame.winfo_toplevel().lift()
                        return False
                if 'position' in self.form[1][column] and column in self.table.model.df.columns:
                    if self.table.model.df.columns.get_loc(column) + 1 != self.form[1][column]['position']:
                        self.validation_label.config(image=self.invalid_icon)
                        mb.showinfo('Invalid position',
                                    'The column ' + column + ' is not at the right position.\n'
                                    'The column should be at the emplacement ' + str(self.form[1][column]['position']))
                        self.frame.winfo_toplevel().lift()
                        return False
                if column in self.table.model.df.columns and 'valid condition' in self.form[1][column]:
                    for i, cell in enumerate(self.table.model.df[column]):
                        is_null = pd.isnull(cell)
                        if not is_null and not cell == '':
                            if not EntryManager.text_to_condition(self.form[1][column]['valid condition'], cell):
                                self.validation_label.config(image=self.invalid_icon)
                                # If the condition is not met, create a popup to inform the user.
                                mb.showinfo('Invalid condition',
                                            'The condition for the column ' + column + ' is not met.\n' +
                                            self.form[1][column]['readable condition'] + '\n' + 'The value is: ' + str(cell))
                                self.frame.winfo_toplevel().lift()
                                return False

            # Drop the empty column.
            if '' in self.table.model.df.columns:
                self.table.model.df.drop('', axis=1, inplace=True)
            is_null_df = self.table.model.df.isnull()
            # Check if all rows where the first cell is not empty have all cells filled.
            for i in range(len(self.table.model.df)):
                if not is_null_df. iloc[i][0]:
                    if is_null_df.iloc[i].any():
                        self.validation_label.config(image=self.invalid_icon)
                        mb.showinfo('Invalid condition',
                                    'A value is empty.\n' +
                                    'Please fill the cell with n/a if it is expected or fill the cell.')
                        self.frame.winfo_toplevel().lift()
                        return False
                else:
                    if not is_null_df.iloc[i].all():
                        self.validation_label.config(image=self.invalid_icon)
                        mb.showinfo('Invalid condition',
                                    'A value is empty.\n' +
                                    'Please fill the cell with n/a if it is expected or fill the cell.')
                        self.frame.winfo_toplevel().lift()
                        return False
            self.validation_label.config(image=self.valid_icon)
            self.table.redraw()
            return True
        else:
            return True

    def save_form(self, path):
        """This method is used to save the form.

        :param path: str
            The path where the form will be saved.
        """
        if '.json' in self.form[0]:
            self.save_form_json(path)
        elif '.tsv' in self.form[0]:
            self.save_form_tsv(path)
        elif '.md' in self.form[0]:
            self.save_form_md(path)

    def save_form_json(self, path):
        """This method is used to save the form in a json file.

        :param path: str
            The path where the form will be saved.
        """
        data = {}
        for entry in self.entries:
            if not entry.get_text() == '' and not entry.get_text() == [''] and not entry.get_text() == {''}:
                data[entry.get_name()] = entry.get_text()
        with open(os.path.join(path, self.form[0]), 'w') as file:
            json.dump(data, file)

    def save_form_md(self, path):
        """This method is used to save the form in a markdown file.

        :param path: str
            The path where the form will be saved.
        """
        with open(os.path.join(path, self.form[0]), 'w') as file:
            file.write(self.entries[0].get('1.0', tk.END))

    def create_form_tsv(self):
        """This method is used to create a form for a tsv file.
        """
        table_frame = tk.Frame(self.frame)
        table_frame.grid(row=self.current_row, column=0, columnspan=5)
        # Set the columns of the table for each required column in the form.
        required_columns = []
        recommendation = []
        for i, column in enumerate(self.form[1]):
            if self.form[1][column]['requirement level'] == 'required':
                required_columns.append(column)
            else:
                recommendation.append(column)
        dataframe = pd.DataFrame(columns=required_columns)
        dataframe.loc[0] = ''
        self.table = Table(table_frame, showtoolbar=True, showstatusbar=True, editable=True, dataframe=dataframe)
        # Add an empty column to the dataframe to allow the user to add more columns.
        self.table.model.df[''] = ''
        self.table.addRows(10)
        self.table.show()
        self.confirmation_button = tk.Button(table_frame, text='Test Validation', command=self.notify)
        self.confirmation_button.grid(row=self.current_row + 1, column=1, columnspan=3)

        self.validation_label = tk.Label(table_frame, image=self.invalid_icon)
        self.validation_label.grid(row=self.current_row + 1, column=4)
        create_tool_tip(self.validation_label, 'Every required field must be filled to validate the form.\n'
                                             'The row must be filled entirely in each named column.\n'
                                             'If an information is missing, fill the cell with n/a.')

        # Display the recommendation as text at the right of the table.
        recommendation_frame = tk.Frame(self.frame)
        recommendation_frame.grid(row=0, column=6, rowspan=10)
        null_to_na_button = tk.Button(recommendation_frame, text='Replace empty cells by n/a', command=self.null_to_na_tsv)
        null_to_na_button.pack()

        recommendation_label = tk.Label(recommendation_frame, text='Recommendation:', font=('Arial', 12, 'bold'))
        recommendation_label.pack()
        recommendation_text = tk.Text(recommendation_frame, height=10, width=50)
        recommendation_text.pack()
        recommendation_text.insert(tk.END, 'Recommendation:\n')
        for column in recommendation:
            # Add the column name with the good style in accordance to its requirement level and description to the recommendation text.
            recommendation_text.insert(tk.END, column + ': ' + self.form[1][column]['description'] + '\n',
                                       self.name_style[self.form[1][column]['requirement level']])
        recommendation_text.config(state=tk.DISABLED)

    def null_to_na_tsv(self):
        """This method is used to replace the empty cells or '' cells of the table by 'n/a'.

        Only the empty cells in a row where at least one cell is filled will be replaced.
        """
        is_null_df = self.table.model.df.isnull()
        for i in range(len(self.table.model.df)):
            if not is_null_df.iloc[i].all():
                for j in range(len(self.table.model.df.columns)):
                    if is_null_df.iloc[i][j] or self.table.model.df.iloc[i, j] == '':
                        self.table.model.df.iloc[i, j] = 'n/a'
        self.table.redraw()

    def save_form_tsv(self, path):
        """This method is used to save the form in a tsv file.

        :param path: str
            The path where the form will be saved.
        """
        # Remove the rows where the first cell is empty or "".
        bool_df = self.table.model.df.isnull()
        self.table.model.df = self.table.model.df[~bool_df.iloc[:, 0]]
        self.table.model.df = self.table.model.df[self.table.model.df.iloc[:, 0] != '']

        self.table.model.df.to_csv(os.path.join(path, self.form[0]), sep='\t', index=False, lineterminator='\n')

    def notify(self):
        """This method is used to notify the application that the state of an entry has changed.
        """
        self.frame.winfo_toplevel().event_generate('<<EntryStateChanged>>')

    def import_data(self, path):
        """This method is used to import data in the form.

        :param path: str
            The path of the file containing the data.
        """
        if '.tsv' in self.form[0]:
            self.import_data_tsv(path)
        elif '.json' in self.form[0]:
            self.import_data_json(path)
        elif '.md' in self.form[0]:
            self.import_data_md(path)

    def import_data_json(self, path):
        """This method is used to import data in the form from a json file.

        :param path: str
            The path of the file containing the data.
        """
        with open(path, 'r') as file:
            data = json.load(file)
        for entry in self.entries:
            if entry.get_name() in data:
                entry.set_text(data[entry.get_name()])
        self.notify()

    def import_data_md(self, path):
        """This method is used to import data in the form from a markdown file.

        :param path: str
            The path of the file containing the data.
        """
        with open(path, 'r') as file:
            data = file.read()
        self.entries[0].insert(tk.END, data)
        self.notify()

    def import_data_tsv(self, path):
        """This method is used to import data in the form from a tsv file.

        :param path: str
            The path of the file containing the data.
        """
        if path.split('.')[-1] == 'tsv':
            self.table.model.df = pd.read_csv(path, sep='\t')
        elif path.split('.')[-1] == 'csv':
            self.table.model.df = pd.read_csv(path)
        elif path.split('.')[-1] == 'xlsx':
            self.table.model.df = pd.read_excel(path)
        else:
            mb.showinfo('Invalid file',
                        'The file must be a tsv, csv or xlsx file.')
            return
        self.table.redraw()
        self.confirmation_button.grid_forget()
        self.validation_label.grid_forget()
        self.confirmation_button.grid(row=self.current_row + 1, column=1, columnspan=3)
        self.validation_label.grid(row=self.current_row + 1, column=4)
        self.notify()


class FormHandler:
    """This class is used to handle multiple forms and facilitate their management.

    Attributes:
        frame: Frame
            The frame containing the forms.
        form_frame_list: list
            A list containing the forms.
        file: dict
            A dictionary containing the forms.
        help_icon: PhotoImage
            The icon used to display a help message.
        invalid_icon: PhotoImage
            The icon used to display an invalid message.
        valid_icon: PhotoImage
            The icon used to display a valid message.

    Methods:
        show_forms():
            This method is used to show a specific form.
        import_data():
            This method is used to import data in a form.
        save_forms():
            This method is used to save a form.
    """

    def __init__(self, frame, necessary_forms, file_path):
        """The constructor of the class, it creates a form handler based on the file given in parameter.

        :param frame: Frame
            The frame containing the forms.
        :param necessary_forms: list
            A list containing the names of the forms that will be created.
        :param file_path: str
            The path of the json file containing the forms specifications.
        """
        self.frame = frame
        self.form_frame_list = []
        self.file = json.load(open(os.path.join(os.path.dirname(__file__), file_path)))

        with Image.open(os.path.join(os.path.dirname(__file__), 'res/help_icon.png')) as img_help:
            img_help = img_help.resize((20, 20))
            self.help_icon = ImageTk.PhotoImage(img_help)
        with Image.open(os.path.join(os.path.dirname(__file__), 'res/invalid_icon.png')) as img_invalid:
            img_invalid = img_invalid.resize((20, 20))
            self.invalid_icon = ImageTk.PhotoImage(img_invalid)
        with Image.open(os.path.join(os.path.dirname(__file__), 'res/valid_icon.png')) as img_valid:
            img_valid = img_valid.resize((20, 20))
            self.valid_icon = ImageTk.PhotoImage(img_valid)

        for form in self.file.items():
            if form[0] in necessary_forms:
                self.form_frame_list.append(Form(form, self.frame, self.help_icon, self.invalid_icon, self.valid_icon))

    def show_forms(self, index, row=1, column=0, sticky='nsew'):
        """This method is used to show a specific form.

        :param index: int
            The index of the form to show.
        :param row: int
            The row where the form will be displayed.
        :param column: int
            The column where the form will be displayed.
        :param sticky: str
            The sticky parameter of the form.
        """
        for i, form in enumerate(self.form_frame_list):
            if i == index:
                form.frame.pack(anchor='center', padx=10, pady=10, fill='both', expand=True)
            else:
                form.frame.pack_forget()

    def import_data(self, index, path):
        """This method is used to import data in a form.

        :param index: int
            The index of the form where the data will be imported.
        :param path: str
            The path of the file containing the data.
        """
        self.form_frame_list[index].import_data(path)

    def save_forms(self, index, path):
        """This method is used to save a form.

        :param index: int
            The index of the form to save.
        :param path: str
            The path where the form will be saved.
        """
        self.form_frame_list[index].save_form(path)
        if '.tsv' in self.form_frame_list[index].form[0]:
            for i, form in enumerate(self.form_frame_list):
                if 'linked_file' in form.form[1] and form.form[1]['linked_file'] == self.form_frame_list[index].form[0]:

                    # Create a new form based on the fields of the tsv file with all the requirement level set to recommended.
                    # And the form name set to the name of the tsv file with json type.
                    new_form = (self.form_frame_list[index].form[0].replace('.tsv', '.json'), {})

                    for column in self.form_frame_list[index].table.model.df.columns:
                        new_form[1][column] = {'description': 'Describe what this field correspond to',
                                               'requirement level': 'recommended',
                                               'multiple': False,
                                               'valid condition': 'len(value) > 0'

                                               }
                    # Then replace the form in the form_frame_list with the new form.

                    self.form_frame_list[i] = Form(new_form, self.frame, self.help_icon, self.invalid_icon,
                                                   self.valid_icon)
