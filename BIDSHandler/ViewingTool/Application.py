""" This module contains the Application class.

This class is the main class of the BIDS viewer package. It is used to create the main window of the application.

Classes:
    Application:
        This class is the main class of the BIDS viewer package. It is used to create the main window of the application.

Methods:
    None
"""

import json
import os
import tkinter as tk
import tkinter.ttk as ttk
import warnings
from re import search
from tkinter import filedialog

from PIL import Image, ImageTk
from bids import BIDSLayout

import BIDSHandler.MainApplication as MainApplication
from BIDSHandler.Tools import clean_frame, process_directory
from BIDSHandler.ViewingTool.FileHandler import FileHandler


# noinspection PyAttributeOutsideInit
class Application(tk.Frame):
    """ This class is the main class of the BIDS viewer package. It is used to create the main window of the application.

    Attributes:
        first_launch: bool
            A boolean to know if the application is launched for the first time.
        menu_bar: Menubutton
            The menubar of the application.
        file_menu: Menu
            The file menu of the application it is linked to the menubar.
        select_button: Button
            The button to select the project directory it is displayed when the application is launched.
        project_path: str
            The path of the project directory.
        usable_project_path: str
            The path of the project directory without the last part of the path.
        project_name: str
            The name of the project.
        layout: BIDSLayout
            The layout of the project.
        panedwindow: Panedwindow
            The panedwindow of the application it is the main window of the application.
        left_frame: Frame
            The left frame of the panedwindow, it contains the project treeview.
        tree: Treeview
            The treeview of the project directory.
        sub_icon: PhotoImage
            The icon for the subjects displayed in the treeview.
        ses_icon: PhotoImage
            The icon for the sessions displayed in the treeview.
        folder_icon: PhotoImage
            The icon for the folders displayed in the treeview.
        detail_frame: Frame
            The detail frame of the panedwindow, it contains the project information at start
            and the file information when a file is selected.
        file_handler: FileHandler
            The file handler of the application it is used to display the file information.
        selection: list
            The list of the selected items in the treeview.

    Methods:
        __init__(root):
            The constructor of the class
        select_project():
            This method is used to select the project directory.
        create_left_frame(path):
            This method is used to create the left frame of the application.
        on_select(event):
            This method is used to display the file information when a file is selected in the treeview.
        prepare_search(query, item=''):
            A method to prepare the search in the treeview.
        search(query, item):
            A recursive method to search in the treeview.
        process_directory(parent, path):
            A recursive method to populate the treeview with the project directory.
        create_detail_frame():
            This method is used to create the detail frame of the application and add the project information.

    Necessary libraries:
        - json
        - os
        - tkinter as tk
        - tkinter.ttk as ttk
        - re.search
        - PIL.Image
        - PIL.ImageTk
        - bids.BIDSLayout
        - BIDSHandler.FileHandler.FileHandler
        - tkinter.filedialog
        - warnings
    """

    def __init__(self, root):
        """ The constructor of the class, it instantiates the main window of the application.

        :param root: Tk
            The root window of the application.
        """

        tk.Frame.__init__(self, root)
        self.root = root
        self.menu_with_main_frame = tk.Frame(self.root)
        self.menu_with_main_frame.pack(anchor='center', fill='both', expand=True)
        self.main_frame = tk.Frame(self.menu_with_main_frame)

        # Ignore expected warnings
        warnings.filterwarnings("ignore", category=DeprecationWarning, module='mne')
        warnings.filterwarnings("ignore", category=FutureWarning, module='mne')
        warnings.filterwarnings("ignore", category=RuntimeWarning, module='mne')
        warnings.filterwarnings("ignore", category=FutureWarning, module='tkinter')

        self.create_widgets()

    def create_widgets(self):
        """ This method is used to create the widgets of the main window of the application.

        It creates the menubar and the file menu.
        It also creates the select button to select the project directory.
        """

        self.menu_bar = ttk.Menubutton(self.menu_with_main_frame, text='File')
        self.menu_bar.pack(anchor='nw')

        self.file_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.file_menu.add_command(label='Select Project', command=self.select_project)
        self.file_menu.add_command(label='Tool Selection', command=self.tool_selection)
        self.menu_bar['menu'] = self.file_menu

        self.main_frame.pack(anchor='center', fill='both', expand=True)

        self.button_frame = tk.Frame(self.main_frame)
        self.button_frame.pack(anchor='center', fill='none', expand=True)

        self.select_button = tk.Button(self.button_frame, text='Select Project', command=self.select_project)
        self.select_button.pack(anchor='center', pady=10)

        back_button = tk.Button(self.button_frame, text='Back to Tool Selection', command=self.tool_selection)
        back_button.pack(anchor='center', pady=10)



        self.select_button.config(height=3, width=20, relief='solid')
        back_button.config(height=3, width=20, relief='solid')

    def tool_selection(self):
        """ This method is used to go back to the tool selection.

        The user is guided back to the tool selection.
        """
        self.menu_with_main_frame.destroy()
        main_application = MainApplication.Application(self.root)
        main_application.pack()
        main_application.mainloop()

    def select_project(self):
        """ This method is used to select the project directory and launch the building of the application.

        It opens a dialog to select the project directory and then creates the treeview of the project directory.
        If the user cancels the dialog, nothing happens.
        """

        self.project_path = filedialog.askdirectory()

        if not self.project_path:
            return

        # Clear the root window
        clean_frame(self.main_frame)

        self.layout = BIDSLayout(self.project_path)

        # Get the project name from the dataset_description.json file
        project_data_path = self.layout.root + '/dataset_description.json'
        with open(project_data_path, 'r') as file:
            project_data = json.load(file)
            self.project_name = project_data['Name']

        if self.project_name == '':
            self.project_name = 'Project'

        # Remove the last part of the path to get the project path
        self.usable_project_path = os.path.dirname(self.project_path) + '/'

        self.panedwindow = ttk.Panedwindow(self.main_frame, orient='horizontal')
        self.panedwindow.pack(fill='both', expand=True, anchor='center')

        self.create_left_frame()
        self.create_detail_frame()

        self.file_handler = FileHandler(self.layout, self.detail_frame)

        self.menu_with_main_frame.pack(anchor='center', fill='both', expand=True)

    def create_left_frame(self):
        """ This method is used to create the left frame of the application.

        It creates the treeview of the project directory and populates it with the project directory.
        It also adds the icons to the treeview, a search bar and a search button.
        The treeview is linked to the detail frame to display the file information when a file is selected.
        """

        self.left_frame = ttk.Frame(self.panedwindow)
        self.left_frame.north_frame = ttk.Frame(self.left_frame)
        self.left_frame.north_frame.pack(fill='x')

        # Create the search entry and button
        search_entry = ttk.Entry(self.left_frame.north_frame)
        search_entry.grid(row=0, column=0, sticky='we')
        search_button = ttk.Button(self.left_frame.north_frame, text='Search',
                                   command=lambda: self.prepare_search(query=search_entry.get()))
        search_button.grid(row=0, column=1, sticky='we')

        # Create the treeview
        self.tree = ttk.Treeview(self.left_frame)
        self.tree.pack(fill='both', expand=True)
        self.tree.heading('#0', text=self.project_name, anchor='w')

        # Create the scrollbar and link it to the treeview
        ysb = ttk.Scrollbar(self.left_frame, orient='vertical', command=self.tree.yview)
        # noinspection PyArgumentList
        self.tree.configure(yscroll=ysb.set)

        # Load icons for the treeview
        with Image.open(os.path.join(os.path.dirname(__file__), 'res/subIcon.png')) as img_sub:
            self.sub_icon = ImageTk.PhotoImage(img_sub)

        with Image.open(os.path.join(os.path.dirname(__file__), 'res/sesIcon.png')) as img_ses:
            img_ses = img_ses.resize((20, 20))
            self.ses_icon = ImageTk.PhotoImage(img_ses)

        with Image.open(os.path.join(os.path.dirname(__file__), 'res/folder.png')) as img_folder:
            img_folder = img_folder.resize((20, 20))
            self.folder_icon = ImageTk.PhotoImage(img_folder)

        # Populate the treeview with the project directory and add icons
        abspath = os.path.abspath(self.project_path)
        root_node = self.tree.insert('', 'end', text=os.path.basename(self.project_path), image=self.folder_icon, open=True)
        process_directory(root_node, abspath, self.tree)
        self.tree.tag_configure('sub', image=self.sub_icon)
        self.tree.tag_configure('ses', image=self.ses_icon)

        self.tree.bind('<<TreeviewSelect>>', self.on_select)

        self.panedwindow.add(self.left_frame)
        self.panedwindow.pack(fill='both', expand=True, anchor='center')

    # noinspection PyUnusedLocal
    def on_select(self, event):
        """ This method is used to display the file information when a file is selected in the treeview.

        It gets the path of the selected item in the treeview and displays the file information in the detail frame.
        If multiple items are selected, nothing happens.
        The file_handler is used to display the file information.

        :param event: Event
            The event of the selection in the treeview.
        """

        if len(self.tree.selection()) > 1:
            return

        item = self.tree.selection()[0]

        path = self.tree.item(item, 'text')
        parent = self.tree.parent(item)
        while parent:
            path = self.tree.item(parent, 'text') + '/' + path
            parent = self.tree.parent(parent)

        path = self.usable_project_path + path
        path = path.replace('\\', '/')
        self.file_handler.display_file(path)

    def prepare_search(self, query, item=''):
        """ A method to prepare the search in the treeview.

        It clears the selection list and calls the search method.
        Then it highlights the items found by the search method items in the treeview.

        :param query: str
            The query to search in the treeview.
        :param item: str
            The item to start the search from.
        """

        self.selection = []
        self.search(query, item)
        self.tree.selection_set(self.selection)

    def search(self, query, item):
        """ A recursive method to search in the treeview.

        It searches in the treeview for the query in the filename of the items.
        If the item is a leaf, it checks if its text contains the query.
        If it does, it opens the item and its parents up to the root and highlights the item.
        If the item is a branch, it searches in its children.

        :param query: str
            The query to search in the treeview.
        :param item: str
            The item to search from.
        """

        # If the item is not specified, search from the root
        if item == '':
            item = self.tree.get_children()

        if not self.tree.get_children(item):
            if query.lower() in self.tree.item(item)['text'].lower():
                self.tree.item(item, open=True)
                parent = self.tree.parent(item)
                while parent:
                    self.tree.item(parent, open=True)
                    parent = self.tree.parent(parent)
                self.selection.append(item)
            else:
                self.tree.item(item, open=False)

        else:
            for child in self.tree.get_children(item):
                self.search(query, child)

    def create_detail_frame(self):
        """ This method is used to create the detail frame of the application and add the project information.

        It creates the detail frame and adds the project information to it.
        The project information includes its name, the number of subjects, sessions, runs and tasks.
        """

        self.detail_frame = tk.Frame(self.panedwindow)
        subject_string = 'Number of subjects : ' + str(len(self.layout.get_subjects()))
        session_string = 'Number of sessions : ' + str(len(self.layout.get_sessions()))
        run_string = 'Number of runs : ' + str(len(self.layout.get_runs()))
        task_string = 'Tasks : ' + str(self.layout.get_tasks())

        project_info = ttk.Label(self.detail_frame, text=self.project_name
                                                         + '\n' + subject_string
                                                         + '\n' + session_string
                                                         + '\n' + run_string
                                                         + '\n' + task_string)
        project_info.pack(anchor='center', expand=True, fill='y', side='top')

        # Currently not working because mne_bids needs the sidecar files to be in the same folder as the data
        # Thus not following the BIDS standard notably for the inheritance
        #   project_report = mne_bids.make_report(self.project_path)
        #   project_report_label = ttk.Label(self.detail_frame, text=project_report)
        #   project_report_label.pack(anchor='center', expand=True, fill='y', side='top')

        self.panedwindow.add(self.detail_frame)
        self.panedwindow.pack(fill='both', expand=True, anchor='center')

