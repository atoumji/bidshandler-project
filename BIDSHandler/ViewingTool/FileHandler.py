""" This module is responsible for dictating how to handle each file type.

Classes:
    FileHandler:
        This class is responsible for dictating how to handle each file type.

Functions:
    clean_frame():
        This function is used to clean a frame.
    text_from_json():
        This function is used to create a text widget from a json object.
"""

import json
import os
import tkinter as tk
import tkinter.ttk as ttk
from re import search

import mne
import mne_bids
from PIL import Image, ImageTk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from nilearn import image, plotting
from pandastable import Table
from tkinter import messagebox as mb

from BIDSHandler.Tools import Tools, clean_frame, search_inheritance


def text_from_json(data, frame):
    """ This function is used to create a text widget from a json object.

    This function is used to create a text widget from a json object.
    The text widget is disabled and therefore cannot be modified.

    :param data: dict
        The json object to display
    :param frame: tk.Frame
        The frame in which to display the text widget
    :return: tk.Text
        The text widget created from the json object
    """

    data = json.dumps(data,
                      skipkeys=True,
                      allow_nan=True,
                      indent=6)
    data = tk.StringVar(value=data)
    metadata_label = tk.Text(frame, height=50, width=100)
    metadata_label.insert(tk.END, data.get())
    metadata_label.configure(state='disabled')
    return metadata_label


class FileHandler:
    """ This class is responsible for dictating how to handle each file type.

    Attributes:
        layout: BIDSLayout
            The BIDS layout of the project
        display_frame: tk.Frame
            The frame in which to display the files

    Methods:
        __init__():
            The constructor of the class, it instantiates the file handler.
        create_top_window():
            Create a top window
        display_file():
            Display a file in the detail frame
        display_not_implemented():
            Display a message saying that the action is not implemented yet
        display_nifti3d():
            Display a nifti image in the detail frame
        display_nifti4d():
            Display a 4D nifti image in the detail frame
        display_json():
            Display a json file in the detail frame
        display_tsv():
            Display a tsv file in the detail frame
        display_tsv_with_metadata():
            Display a tsv file with its metadata at its right in the detail frame
        display_edf():
            Display an edf file in the detail frame
        display_psd_edf():
            Display the PSD of an edf file in the detail frame
        display_raw_edf():
            Display the raw data of an edf file in the detail frame
        add_button():
            Add all the supplementary information buttons
        display_help_mne():
            Open a new window displaying the shortcut help of mne
        display_metadata():
            Open a new window displaying the metadata of the file
        display_events():
            Open a new window displaying the events of the file
        display_subject():
            Open a new window displaying the subject of the file
        display_channels():
            Open a new window displaying the channels of the file
    """

    def __init__(self, layout, display_frame):
        """ The constructor of the class, it instantiates the file handler.

        :param layout: BIDSLayout
            The BIDS layout of the project
        :param display_frame: tk.Frame
            The frame in which to display the files
        """

        self.layout = layout
        self.project = layout.root
        self.display_frame = display_frame
        # Create the tools
        self.tools = Tools(self.layout)

    def create_top_window(self, title):
        """ Create a top window

        This method is used to create a top window.
        The top window is a new window that is created on top of the main window and that can be resized.

        :param title: str
            The title of the top window
        :return: tk.Toplevel
            The top window created
        """

        top_window = tk.Toplevel(self.display_frame.winfo_toplevel())
        top_window.title(title)
        top_window.geometry('725x720')
        top_window.resizable(True, True)
        return top_window

    def display_file(self, path):
        """ Display a file in the detail frame

        This method is used to display a file in the detail frame.
        The file is displayed according to its type.

        :param path: str
            The path of the file to display
        """

        # Get the file type
        extension = os.path.splitext(path)[1]

        # Display the file in the detail frame
        if search('.nii', path):
            dimension = image.load_img(path).shape
            dimension = dimension.__len__()

            if dimension == 3:
                self.display_nifti3d(path)
            elif dimension == 4:
                self.display_nifti4d(path)
        elif extension == '.json':
            self.display_json(path)
        elif extension == '.tsv':
            self.display_tsv(path)
        elif extension == '.edf':
            self.display_edf(path)
        else:
            self.display_not_implemented()

    def display_not_implemented(self):
        """ Display a message saying that the action is not implemented yet

        This method is used to display a message saying that the action is not implemented yet.
        The message is displayed in the display frame.
        """

        clean_frame(self.display_frame)
        not_implemented_label = ttk.Label(self.display_frame, text='This action is not implemented yet.')
        not_implemented_label.pack()

    def display_nifti3d(self, path):
        """ Display a nifti image in the display frame

        This method is used to display a nifti image in the display frame.
        The image is displayed in the right part of the display frame.
        The metadata of the image is displayed in the left part of the display frame.

        :param path: str
            The path of the nifti image to display
        """

        clean_frame(self.display_frame)

        metadata_frame = tk.Frame(self.display_frame)
        metadata_frame.pack(side='left')

        layout_path = path.replace('/', '\\')
        metadata = self.layout.get()[self.layout.get(return_type='filename').index(layout_path)].get_metadata()
        metadata_label = text_from_json(metadata, metadata_frame)

        metadata_label.grid(row=0, column=0)

        scroll = tk.Scrollbar(metadata_frame, orient='vertical', command=metadata_label.yview)
        scroll.grid(row=0, column=1, sticky='ns')
        metadata_label.config(yscrollcommand=scroll.set)

        # Load the nifti image
        fig = Figure(figsize=(5, 5))
        plotting.plot_stat_map(path, figure=fig)

        canvas = FigureCanvasTkAgg(fig, master=self.display_frame)
        canvas.get_tk_widget().pack(side='right', fill='both', expand=True)
        canvas.draw()

    def display_nifti4d(self, path):
        # To Do
        pass

    def display_json(self, path):
        """ Display a json file in the display frame

        This method is used to display a json file in the display frame.
        The content of the json file is displayed in the display frame.

        :param path: str
            The path of the json file to display
        """

        clean_frame(self.display_frame)

        # Open the json file and display the content in the detail frame
        with open(path, 'r') as file:
            content = file.read()
            json_label = ttk.Label(self.display_frame, text=content)
            json_label.pack()

    def display_tsv(self, path, parent=None):
        """ Display a tsv file in the parent frame

        This method is used to display a tsv file in the parent frame.
        It displays the content of the tsv file as a table in the display frame.
        If the parent frame is not specified, the tsv file is displayed in the display frame.
        If there is metadata associated with the tsv file, a button is added to display the metadata.

        :param path: str
            The path of the tsv file to display
        :param parent: tk.Frame (default=None)
            The parent frame in which to display the tsv file
        """

        if parent is None:
            parent = self.display_frame

        clean_frame(parent)

        # Create a frame to display the tsv file
        frame = tk.Frame(parent)
        frame.pack(side='left', expand=True, fill='both')

        # Create a button to display the tsv file with its metadata
        json_button = tk.Button(parent, text='Display with JSON',
                                 command=lambda: self.display_tsv_with_metadata(path, parent))
        if self.tools.pybids_get_metadata_from_path(path).__len__() == 2:
            json_button.configure(state='disabled')
        json_button.pack(anchor='center', expand=True, side='right')
        json_button.configure(height=3, width=20, relief='solid')

        info = self.tools.pybids_path(path)
        info = info.get_df()
        table = Table(frame, showtoolbar=True, showstatusbar=True, dataframe=info)
        table.show()

    def display_tsv_with_metadata(self, path, parent):
        """ Display a tsv file with its metadata in the parent frame

        This method is used to display a tsv file with its metadata in the parent frame.
        It displays the content of the tsv file as a table in the left part of the parent frame.
        The metadata of the tsv file is displayed in the right part of the parent frame.

        :param path: str
            The path of the tsv file to display
        :param parent: tk.Frame
            The parent frame in which to display the tsv file
        """

        self.display_tsv(path, parent)

        # Remove the previous json button
        for widget in parent.winfo_children():
            if isinstance(widget, ttk.Button):
                widget.destroy()

        # Add the metadata of the tsv file
        metadata = self.tools.pybids_get_metadata_from_path(path)
        metadata_label = tk.Text(parent, height=50, width=100)
        metadata_label.insert(tk.END, metadata)
        metadata_label.configure(state='disabled')

        metadata_label.pack()

    def display_edf(self, path, file=None):
        """ Display an edf file in the display frame

        This method is used to display an edf file in the display frame.
        It proposes to display the PSD of the file or the raw data.
        If the file is not specified, it is read using mne from the path.

        :param path: str
            The path of the edf file to display
        :param file: mne.io.Raw (default=None)
            The mne raw object of the edf file
        """

        if file is None:
            file = mne.io.read_raw_edf(path)

        clean_frame(self.display_frame)

        button_frame = tk.Frame(self.display_frame)
        button_frame.place(relx=0.5, rely=0.5, anchor='center')

        # Propose to display the PSD of the file or the raw data
        psd_button = tk.Button(button_frame, text='Display PSD', command=lambda: self.display_psd_edf(path, file))
        psd_button.pack(padx=10, pady=10)
        psd_button.configure(height=3, width=20, relief='solid')

        raw_button = tk.Button(button_frame, text='Display raw data',
                                command=lambda: self.display_raw_edf(path, file))
        raw_button.pack(padx=10, pady=10)
        raw_button.configure(height=3, width=20, relief='solid')

    def display_psd_edf(self, path, file):
        """ Display the PSD of an edf file in the display frame

        This method is used to display the PSD of an edf file in the display frame.
        A set of utility buttons is added at the top of the display frame.

        :param path: str
            The path of the edf file to display
        :param file: mne.io.Raw
            The mne raw object of the edf file
        """

        clean_frame(self.display_frame)

        button_frame = tk.Frame(self.display_frame)
        button_frame.pack(side='top')

        self.add_button(button_frame, path, file)

        # Display the PSD of the file
        fig = file.compute_psd(fmax=100).plot()
        canvas = FigureCanvasTkAgg(fig, master=self.display_frame)
        canvas.get_tk_widget().pack(side='right', fill='both', expand=True)
        canvas.draw()

    def display_raw_edf(self, path, file):
        """ Display the raw data of an edf file in the display frame

        This method is used to display the raw data of an edf file in the display frame.
        A set of utility buttons is added at the top of the display frame.

        :param path: str
            The path of the edf file to display
        :param file: mne.io.Raw
            The mne raw object of the edf file
        """

        clean_frame(self.display_frame)

        button_frame = tk.Frame(self.display_frame)
        button_frame.pack(side='top')

        self.add_button(button_frame, path, file)

        file = mne_bids.get_bids_path_from_fname(path)
        file = mne_bids.read_raw_bids(file)

        # Display the raw data of the file
        fig = file.plot(scalings='auto')
        canvas = FigureCanvasTkAgg(fig, master=self.display_frame)
        canvas.get_tk_widget().pack(side='right', fill='both', expand=True)
        canvas.draw()

    def add_button(self, button_frame, path, file):
        """ Add all the supplementary information buttons

        This method is used to add all the supplementary information buttons to the button frame.
        The buttons are used to display the help of mne, the metadata, the events, the subject and the channels of the file.
        each button is associated with a command that displays the corresponding information in a new window.
        A button is also added to return to the data choice.

        :param button_frame: tk.Frame
            The frame in which to add the buttons
        :param path: str
            The path of the file to display
        :param file: mne.io.Raw
            The mne raw object of the file
        """

        help_button = ttk.Button(button_frame, text='Display help', command=lambda: self.display_help_mne())
        help_button.pack(side='left')

        metadata_button = ttk.Button(button_frame, text='Display metadata', command=lambda: self.display_metadata(path))
        metadata_button.pack(side='left')

        events_button = ttk.Button(button_frame, text='Display events', command=lambda: self.display_events(path))
        events_button.pack(side='left')

        subject_button = ttk.Button(button_frame, text='Display subject', command=lambda: self.display_subject(path))
        subject_button.pack(side='left')

        channels_button = ttk.Button(button_frame, text='Display channels', command=lambda: self.display_channels(path))
        channels_button.pack(side='left')

        back_button = ttk.Button(button_frame, text='Back', command=lambda: self.display_edf(path, file))
        back_button.pack(side='left')

    # noinspection PyTypeChecker
    def display_help_mne(self):
        """ Display the shortcut help of mne

        This method is used to display the shortcut help of mne in a new window.
        The help is displayed as an image in the new window.
        """

        help_window = self.create_top_window('Help')

        # The help image is stored in the res folder of the package
        help_image = Image.open(os.path.join(os.path.dirname(__file__), 'res/mneHelp.png'))
        help_image = ImageTk.PhotoImage(help_image)
        help_label = tk.Label(help_window, image=help_image)
        help_label.pack()

        help_window.mainloop()

    def display_metadata(self, path):
        """ Open a new window displaying the metadata of the file

        This method is used to open a new window displaying the metadata of the file.
        The metadata is displayed in json format in the new window.

        :param path: str
            The path of the file for which to display the metadata
        """
        metadata = self.tools.pybids_get_metadata_from_path(path)

        if metadata.__len__() == 2:
            mb.showinfo("Error", "No metadata file found")
            return

        metadata_window = self.create_top_window('Metadata')

        metadata_label = tk.Text(metadata_window, height=50, width=100)
        metadata_label.insert(tk.END, metadata)
        metadata_label.configure(state='disabled')

        metadata_label.pack()

        metadata_window.mainloop()

    def display_events(self, path):
        """ Open a new window displaying the events of the file

        This method is used to open a new window displaying the events of the file.
        The events are displayed in a table in the new window.

        :param path: str
            The path of the file for which to display the events
        """

        events = search_inheritance(path, self.project, 'events')

        if len(events) == 0:
            mb.showinfo("Error", "No events file found")
            return
        else:
            events = events[0]

        events_window = self.create_top_window('Events')

        self.display_tsv(events, events_window)

    def display_subject(self, path):
        """ Open a new window displaying the subject of the file

        This method is used to open a new window displaying the subject of the file.
        The subject is displayed in json format in the new window.

        :param path: str
            The path of the file for which to display the subject
        """

        raw = mne_bids.read_raw_bids(mne_bids.get_bids_path_from_fname(path))
        subject_window = self.create_top_window('Subject')

        subject = raw.info['subject_info']
        subject_label = text_from_json(subject, subject_window)

        subject_label.pack()

        subject_window.mainloop()

    def display_channels(self, path):
        """ Open a new window displaying the channels of the file

        This method is used to open a new window displaying the channels of the file.
        The channels are displayed in a table in the new window.

        :param path: str
            The path of the file for which to display the channels
        """

        channels = search_inheritance(path, self.project, 'channels')

        if len(channels) == 0:
            mb.showinfo("Error", "No channels file found")
            return
        else:
            channels = channels[0]

        channels_window = self.create_top_window('Channels')

        self.display_tsv(channels, channels_window)
