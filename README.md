# BIDS Handler

This project is a Python application that handles Brain Imaging Data Structure (BIDS) datasets. It provides tools for creating, viewing, and validating BIDS datasets.

## Features

- **Viewing Tool**: Allows you to view the structure and content of a BIDS dataset. (Currently only displays edf files)
- **Creation Tool**: Helps you to create a new BIDS dataset or modify an existing one. (Currently only creates eeg datasets)
- **BIDS Validator**: Opens a web browser at the BIDS Validator website to validate a BIDS project.

## Installation

This project requires Python 3.10 or later. You can download Python from [here](https://www.python.org/downloads/).

After installing Python, you can clone this repository to your local machine using the following command:

```bash
git clone https://gitlab.cristal.univ-lille.fr/atoumji/bidshandler-project.git
```

Then, navigate to the project directory: 

```bash
cd bidshandler-project
```



## Required Python Modules

The required Python modules are listed in the `requirements.txt` file. You can install them using the following command:

```bash
pip install -r requirements.txt
```


## Usage

To run the application run the `main.py` script:

```bash
python main.py
```

### Creation tool
To use the creation tool, you will need to have edf experiment files ready.
You can also have sidecar already created for the edf files, if not the tool will help you create them.

The creation tool follow 4 steps:
1. Select the name of the dataset, the root directory and the base files you want to create.
2. Fill the forms to create the base files.
3. Integrate the edf files into the dataset.
4. Integrate or create the sidecar files for the edf files, in this step you hava three options:
    - Create the sidecar files from scratch.
    - Import data and modify it before creating the sidecar files.
    - Integrate an already existing sidecar file.
   

The form completion use a certain visual feedback:
- The fields with a name in bold are required.
- The fields with a normal font for the name are recommended.
- The fields with a name in italic are optional.

Currently, pandastable is used to display and edit tsv files : 
- In pandastable you need to press enter to validate the changes you made in a cell.
- To add a column or modify its name you will need to right-click on the column header and descend into the "Column" section of the dropdown menu.
- You can also import data by pressing the one of the importation buttons on the right
- For tsv files importation you will need to change the file selection to tsv in the explorer window. And then change the separator to \t in the importation window.


### Viewing tool
To use the viewing tool, you will need to have a BIDS dataset ready.
The viewing tool will display the content of the dataset in a tree view at the left of the window.
You can select a file to display its content in the right part of the window.

### BIDS Validator
To use the BIDS Validator, you will need to have a BIDS dataset ready.
The BIDS Validator will open a web browser at the BIDS Validator website.
You will need to upload your dataset to the website to validate it.
The BIDS Validator will check if your dataset is compliant with the BIDS standard and will display the results in the web browser.

## Contributing

TODO
## License

TODO