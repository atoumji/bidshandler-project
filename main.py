""" This module is the main module of the BIDS viewer package. It is used to create the main window of the application.

Classes:
    BIDSViewer:
        This class is the main class of the BIDS viewer package. It is used to create the main window of the application.

Methods:
    None
"""

import tkinter as tk
import platform

from BIDSHandler.MainApplication import Application as MainApplication


class BIDSViewer:
    """ This class is the main class of the BIDS viewer package. It is used to create the main window of the application.

    Attributes:
        app: Application
            The main window of the application.

    Methods:
        __init__():
            The constructor of the class, it instantiates the main window of the application.
        show():
            This method is used to show the main window of the application.

    """

    def __init__(self):
        """ The constructor of the class, it instantiates the main window of the application.
        """
        root = tk.Tk()
        if platform.system() == 'Windows':
            root.state('zoomed')
        else:
            root.attributes('-zoomed', True)
        root.title('BIDS Handler')
        self.app = MainApplication(root)

    def show(self):
        """ This method is used to show the main window of the application.
        """
        self.app.mainloop()


handler = BIDSViewer()
handler.show()
